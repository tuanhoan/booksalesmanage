﻿
namespace QuanLyBanSach.View
{
    partial class frmHome
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frmHome));
            this.dgvHome = new System.Windows.Forms.DataGridView();
            this.flowLayoutPanel3 = new System.Windows.Forms.FlowLayoutPanel();
            this.btnImportCount = new System.Windows.Forms.Button();
            this.btnExportCount = new System.Windows.Forms.Button();
            this.btnInventory = new System.Windows.Forms.Button();
            this.flowLayoutPanel2 = new System.Windows.Forms.FlowLayoutPanel();
            this.gbxDateBegin = new System.Windows.Forms.GroupBox();
            this.dtpImport = new System.Windows.Forms.DateTimePicker();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.gbxEnd = new System.Windows.Forms.GroupBox();
            this.dateTimePicker1 = new System.Windows.Forms.DateTimePicker();
            this.btnFilter = new System.Windows.Forms.Button();
            this.flowLayoutPanel1 = new System.Windows.Forms.FlowLayoutPanel();
            this.btnImport = new System.Windows.Forms.Button();
            this.btnBillDetail = new System.Windows.Forms.Button();
            this.btnBook = new System.Windows.Forms.Button();
            this.btnSupplier = new System.Windows.Forms.Button();
            this.btnCustomer = new System.Windows.Forms.Button();
            this.btnUsers = new System.Windows.Forms.Button();
            ((System.ComponentModel.ISupportInitialize)(this.dgvHome)).BeginInit();
            this.flowLayoutPanel3.SuspendLayout();
            this.flowLayoutPanel2.SuspendLayout();
            this.gbxDateBegin.SuspendLayout();
            this.gbxEnd.SuspendLayout();
            this.flowLayoutPanel1.SuspendLayout();
            this.SuspendLayout();
            // 
            // dgvHome
            // 
            this.dgvHome.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgvHome.Location = new System.Drawing.Point(14, 411);
            this.dgvHome.Margin = new System.Windows.Forms.Padding(5, 4, 5, 4);
            this.dgvHome.Name = "dgvHome";
            this.dgvHome.RowHeadersWidth = 51;
            this.dgvHome.Size = new System.Drawing.Size(1039, 231);
            this.dgvHome.TabIndex = 19;
            // 
            // flowLayoutPanel3
            // 
            this.flowLayoutPanel3.Controls.Add(this.btnImportCount);
            this.flowLayoutPanel3.Controls.Add(this.btnExportCount);
            this.flowLayoutPanel3.Controls.Add(this.btnInventory);
            this.flowLayoutPanel3.Location = new System.Drawing.Point(15, 229);
            this.flowLayoutPanel3.Margin = new System.Windows.Forms.Padding(5, 4, 5, 4);
            this.flowLayoutPanel3.Name = "flowLayoutPanel3";
            this.flowLayoutPanel3.Size = new System.Drawing.Size(1038, 133);
            this.flowLayoutPanel3.TabIndex = 18;
            // 
            // btnImportCount
            // 
            this.btnImportCount.BackColor = System.Drawing.Color.DodgerBlue;
            this.btnImportCount.Font = new System.Drawing.Font("Microsoft Sans Serif", 15F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point);
            this.btnImportCount.ForeColor = System.Drawing.SystemColors.ButtonFace;
            this.btnImportCount.Location = new System.Drawing.Point(106, 4);
            this.btnImportCount.Margin = new System.Windows.Forms.Padding(106, 4, 5, 4);
            this.btnImportCount.Name = "btnImportCount";
            this.btnImportCount.Size = new System.Drawing.Size(262, 117);
            this.btnImportCount.TabIndex = 0;
            this.btnImportCount.Text = "Lượng Nhập:";
            this.btnImportCount.TextAlign = System.Drawing.ContentAlignment.TopLeft;
            this.btnImportCount.UseVisualStyleBackColor = false;
            // 
            // btnExportCount
            // 
            this.btnExportCount.BackColor = System.Drawing.Color.DodgerBlue;
            this.btnExportCount.Font = new System.Drawing.Font("Microsoft Sans Serif", 15F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point);
            this.btnExportCount.ForeColor = System.Drawing.SystemColors.ButtonFace;
            this.btnExportCount.Location = new System.Drawing.Point(378, 4);
            this.btnExportCount.Margin = new System.Windows.Forms.Padding(5, 4, 5, 4);
            this.btnExportCount.Name = "btnExportCount";
            this.btnExportCount.Size = new System.Drawing.Size(262, 117);
            this.btnExportCount.TabIndex = 1;
            this.btnExportCount.Text = "Lượng Xuất:\r\n";
            this.btnExportCount.TextAlign = System.Drawing.ContentAlignment.TopLeft;
            this.btnExportCount.UseVisualStyleBackColor = false;
            // 
            // btnInventory
            // 
            this.btnInventory.BackColor = System.Drawing.Color.DodgerBlue;
            this.btnInventory.Font = new System.Drawing.Font("Microsoft Sans Serif", 15F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point);
            this.btnInventory.ForeColor = System.Drawing.SystemColors.ButtonFace;
            this.btnInventory.Location = new System.Drawing.Point(650, 4);
            this.btnInventory.Margin = new System.Windows.Forms.Padding(5, 4, 5, 4);
            this.btnInventory.Name = "btnInventory";
            this.btnInventory.Size = new System.Drawing.Size(262, 117);
            this.btnInventory.TabIndex = 2;
            this.btnInventory.Text = "Tồn Kho:";
            this.btnInventory.TextAlign = System.Drawing.ContentAlignment.TopLeft;
            this.btnInventory.UseVisualStyleBackColor = false;
            // 
            // flowLayoutPanel2
            // 
            this.flowLayoutPanel2.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.flowLayoutPanel2.Controls.Add(this.gbxDateBegin);
            this.flowLayoutPanel2.Controls.Add(this.gbxEnd);
            this.flowLayoutPanel2.Controls.Add(this.btnFilter);
            this.flowLayoutPanel2.Location = new System.Drawing.Point(14, 120);
            this.flowLayoutPanel2.Margin = new System.Windows.Forms.Padding(5, 4, 5, 4);
            this.flowLayoutPanel2.Name = "flowLayoutPanel2";
            this.flowLayoutPanel2.Size = new System.Drawing.Size(1039, 83);
            this.flowLayoutPanel2.TabIndex = 17;
            // 
            // gbxDateBegin
            // 
            this.gbxDateBegin.Controls.Add(this.dtpImport);
            this.gbxDateBegin.Controls.Add(this.groupBox1);
            this.gbxDateBegin.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point);
            this.gbxDateBegin.Location = new System.Drawing.Point(5, 4);
            this.gbxDateBegin.Margin = new System.Windows.Forms.Padding(5, 4, 5, 4);
            this.gbxDateBegin.Name = "gbxDateBegin";
            this.gbxDateBegin.Padding = new System.Windows.Forms.Padding(5, 4, 5, 4);
            this.gbxDateBegin.Size = new System.Drawing.Size(214, 64);
            this.gbxDateBegin.TabIndex = 2;
            this.gbxDateBegin.TabStop = false;
            this.gbxDateBegin.Text = "Ngày Bắt Đầu";
            // 
            // dtpImport
            // 
            this.dtpImport.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.dtpImport.Location = new System.Drawing.Point(8, 24);
            this.dtpImport.Margin = new System.Windows.Forms.Padding(5, 4, 5, 4);
            this.dtpImport.Name = "dtpImport";
            this.dtpImport.Size = new System.Drawing.Size(131, 23);
            this.dtpImport.TabIndex = 0;
            // 
            // groupBox1
            // 
            this.groupBox1.Location = new System.Drawing.Point(222, 0);
            this.groupBox1.Margin = new System.Windows.Forms.Padding(5, 4, 5, 4);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Padding = new System.Windows.Forms.Padding(5, 4, 5, 4);
            this.groupBox1.Size = new System.Drawing.Size(386, 64);
            this.groupBox1.TabIndex = 3;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "groupBox1";
            // 
            // gbxEnd
            // 
            this.gbxEnd.Controls.Add(this.dateTimePicker1);
            this.gbxEnd.Location = new System.Drawing.Point(229, 4);
            this.gbxEnd.Margin = new System.Windows.Forms.Padding(5, 4, 5, 4);
            this.gbxEnd.Name = "gbxEnd";
            this.gbxEnd.Padding = new System.Windows.Forms.Padding(5, 4, 5, 4);
            this.gbxEnd.Size = new System.Drawing.Size(210, 64);
            this.gbxEnd.TabIndex = 3;
            this.gbxEnd.TabStop = false;
            this.gbxEnd.Text = "Ngày kết thúc";
            // 
            // dateTimePicker1
            // 
            this.dateTimePicker1.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.dateTimePicker1.Location = new System.Drawing.Point(8, 24);
            this.dateTimePicker1.Margin = new System.Windows.Forms.Padding(5, 4, 5, 4);
            this.dateTimePicker1.Name = "dateTimePicker1";
            this.dateTimePicker1.Size = new System.Drawing.Size(130, 27);
            this.dateTimePicker1.TabIndex = 4;
            // 
            // btnFilter
            // 
            this.btnFilter.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(192)))), ((int)(((byte)(128)))));
            this.btnFilter.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point);
            this.btnFilter.Location = new System.Drawing.Point(458, 16);
            this.btnFilter.Margin = new System.Windows.Forms.Padding(14, 16, 14, 16);
            this.btnFilter.Name = "btnFilter";
            this.btnFilter.Size = new System.Drawing.Size(136, 53);
            this.btnFilter.TabIndex = 4;
            this.btnFilter.Text = "Lọc";
            this.btnFilter.UseVisualStyleBackColor = false;
            this.btnFilter.Click += new System.EventHandler(this.btnFilter_Click);
            // 
            // flowLayoutPanel1
            // 
            this.flowLayoutPanel1.Controls.Add(this.btnImport);
            this.flowLayoutPanel1.Controls.Add(this.btnBillDetail);
            this.flowLayoutPanel1.Controls.Add(this.btnBook);
            this.flowLayoutPanel1.Controls.Add(this.btnSupplier);
            this.flowLayoutPanel1.Controls.Add(this.btnCustomer);
            this.flowLayoutPanel1.Controls.Add(this.btnUsers);
            this.flowLayoutPanel1.Location = new System.Drawing.Point(15, 16);
            this.flowLayoutPanel1.Margin = new System.Windows.Forms.Padding(5, 4, 5, 4);
            this.flowLayoutPanel1.Name = "flowLayoutPanel1";
            this.flowLayoutPanel1.Size = new System.Drawing.Size(1038, 96);
            this.flowLayoutPanel1.TabIndex = 16;
            // 
            // btnImport
            // 
            this.btnImport.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(224)))), ((int)(((byte)(192)))));
            this.btnImport.FlatAppearance.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(224)))), ((int)(((byte)(192)))));
            this.btnImport.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point);
            this.btnImport.ForeColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.btnImport.Image = ((System.Drawing.Image)(resources.GetObject("btnImport.Image")));
            this.btnImport.ImageAlign = System.Drawing.ContentAlignment.BottomCenter;
            this.btnImport.Location = new System.Drawing.Point(5, 4);
            this.btnImport.Margin = new System.Windows.Forms.Padding(5, 4, 5, 4);
            this.btnImport.Name = "btnImport";
            this.btnImport.Size = new System.Drawing.Size(161, 77);
            this.btnImport.TabIndex = 6;
            this.btnImport.Text = "Nhập kho";
            this.btnImport.TextAlign = System.Drawing.ContentAlignment.TopCenter;
            this.btnImport.UseVisualStyleBackColor = false;
            this.btnImport.Click += new System.EventHandler(this.btnImport_Click);
            // 
            // btnBillDetail
            // 
            this.btnBillDetail.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(224)))), ((int)(((byte)(192)))));
            this.btnBillDetail.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point);
            this.btnBillDetail.ForeColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.btnBillDetail.Image = ((System.Drawing.Image)(resources.GetObject("btnBillDetail.Image")));
            this.btnBillDetail.ImageAlign = System.Drawing.ContentAlignment.BottomCenter;
            this.btnBillDetail.Location = new System.Drawing.Point(176, 4);
            this.btnBillDetail.Margin = new System.Windows.Forms.Padding(5, 4, 5, 4);
            this.btnBillDetail.Name = "btnBillDetail";
            this.btnBillDetail.Size = new System.Drawing.Size(161, 77);
            this.btnBillDetail.TabIndex = 11;
            this.btnBillDetail.Text = "Xuất Hóa Đơn";
            this.btnBillDetail.TextAlign = System.Drawing.ContentAlignment.TopCenter;
            this.btnBillDetail.UseVisualStyleBackColor = false;
            this.btnBillDetail.Click += new System.EventHandler(this.btnExports_Click);
            // 
            // btnBook
            // 
            this.btnBook.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(224)))), ((int)(((byte)(192)))));
            this.btnBook.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point);
            this.btnBook.ForeColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.btnBook.Image = ((System.Drawing.Image)(resources.GetObject("btnBook.Image")));
            this.btnBook.ImageAlign = System.Drawing.ContentAlignment.BottomCenter;
            this.btnBook.Location = new System.Drawing.Point(347, 4);
            this.btnBook.Margin = new System.Windows.Forms.Padding(5, 4, 5, 4);
            this.btnBook.Name = "btnBook";
            this.btnBook.Size = new System.Drawing.Size(161, 77);
            this.btnBook.TabIndex = 10;
            this.btnBook.Text = "Sách";
            this.btnBook.TextAlign = System.Drawing.ContentAlignment.TopCenter;
            this.btnBook.UseVisualStyleBackColor = false;
            this.btnBook.Click += new System.EventHandler(this.btnBook_Click);
            // 
            // btnSupplier
            // 
            this.btnSupplier.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(224)))), ((int)(((byte)(192)))));
            this.btnSupplier.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point);
            this.btnSupplier.Image = ((System.Drawing.Image)(resources.GetObject("btnSupplier.Image")));
            this.btnSupplier.ImageAlign = System.Drawing.ContentAlignment.BottomCenter;
            this.btnSupplier.Location = new System.Drawing.Point(518, 4);
            this.btnSupplier.Margin = new System.Windows.Forms.Padding(5, 4, 5, 4);
            this.btnSupplier.Name = "btnSupplier";
            this.btnSupplier.Size = new System.Drawing.Size(161, 77);
            this.btnSupplier.TabIndex = 9;
            this.btnSupplier.Text = "Nhà cung cấp";
            this.btnSupplier.TextAlign = System.Drawing.ContentAlignment.TopCenter;
            this.btnSupplier.UseVisualStyleBackColor = false;
            this.btnSupplier.Click += new System.EventHandler(this.btnSupplier_Click);
            // 
            // btnCustomer
            // 
            this.btnCustomer.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(224)))), ((int)(((byte)(192)))));
            this.btnCustomer.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point);
            this.btnCustomer.Image = ((System.Drawing.Image)(resources.GetObject("btnCustomer.Image")));
            this.btnCustomer.ImageAlign = System.Drawing.ContentAlignment.BottomCenter;
            this.btnCustomer.Location = new System.Drawing.Point(689, 4);
            this.btnCustomer.Margin = new System.Windows.Forms.Padding(5, 4, 5, 4);
            this.btnCustomer.Name = "btnCustomer";
            this.btnCustomer.Size = new System.Drawing.Size(161, 77);
            this.btnCustomer.TabIndex = 8;
            this.btnCustomer.Text = "Khách Hàng";
            this.btnCustomer.TextAlign = System.Drawing.ContentAlignment.TopCenter;
            this.btnCustomer.UseVisualStyleBackColor = false;
            this.btnCustomer.Click += new System.EventHandler(this.btnCustomer_Click);
            // 
            // btnUsers
            // 
            this.btnUsers.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(224)))), ((int)(((byte)(192)))));
            this.btnUsers.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point);
            this.btnUsers.ForeColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.btnUsers.Image = ((System.Drawing.Image)(resources.GetObject("btnUsers.Image")));
            this.btnUsers.ImageAlign = System.Drawing.ContentAlignment.BottomCenter;
            this.btnUsers.Location = new System.Drawing.Point(860, 4);
            this.btnUsers.Margin = new System.Windows.Forms.Padding(5, 4, 5, 4);
            this.btnUsers.Name = "btnUsers";
            this.btnUsers.Size = new System.Drawing.Size(161, 77);
            this.btnUsers.TabIndex = 7;
            this.btnUsers.Text = "Người Dùng";
            this.btnUsers.TextAlign = System.Drawing.ContentAlignment.TopCenter;
            this.btnUsers.UseVisualStyleBackColor = false;
            this.btnUsers.Click += new System.EventHandler(this.btnUsers_Click);
            // 
            // frmHome
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 20F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1091, 644);
            this.Controls.Add(this.dgvHome);
            this.Controls.Add(this.flowLayoutPanel3);
            this.Controls.Add(this.flowLayoutPanel2);
            this.Controls.Add(this.flowLayoutPanel1);
            this.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.Name = "frmHome";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "frmHome";
            ((System.ComponentModel.ISupportInitialize)(this.dgvHome)).EndInit();
            this.flowLayoutPanel3.ResumeLayout(false);
            this.flowLayoutPanel2.ResumeLayout(false);
            this.gbxDateBegin.ResumeLayout(false);
            this.gbxEnd.ResumeLayout(false);
            this.flowLayoutPanel1.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.DataGridView dgvHome;
        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel3;
        private System.Windows.Forms.Button btnImportCount;
        private System.Windows.Forms.Button btnExportCount;
        private System.Windows.Forms.Button btnInventory;
        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel2;
        private System.Windows.Forms.GroupBox gbxDateBegin;
        private System.Windows.Forms.DateTimePicker dtpImport;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.GroupBox gbxEnd;
        private System.Windows.Forms.DateTimePicker dateTimePicker1;
        private System.Windows.Forms.Button btnFilter;
        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel1;
        private System.Windows.Forms.Button btnImport;
        private System.Windows.Forms.Button btnBillDetail;
        private System.Windows.Forms.Button btnBook;
        private System.Windows.Forms.Button btnSupplier;
        private System.Windows.Forms.Button btnCustomer;
        private System.Windows.Forms.Button btnUsers;
    }
}