﻿
namespace QuanLyBanSach.View
{
    partial class frmCustomer
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.panel1 = new System.Windows.Forms.Panel();
            this.dgvCustomer = new System.Windows.Forms.DataGridView();
            this.dtpCreate_atCustomer = new System.Windows.Forms.DateTimePicker();
            this.tbxAddressCustomer = new System.Windows.Forms.TextBox();
            this.lbAddressCustomer = new System.Windows.Forms.Label();
            this.lbCreate_atCustomer = new System.Windows.Forms.Label();
            this.tbxNameCustomer = new System.Windows.Forms.TextBox();
            this.lbNameCustomer = new System.Windows.Forms.Label();
            this.tbxEmailCustomer = new System.Windows.Forms.TextBox();
            this.lbEmailCustomer = new System.Windows.Forms.Label();
            this.tbxPhoneCustomer = new System.Windows.Forms.TextBox();
            this.lbPhoneCustomer = new System.Windows.Forms.Label();
            this.tbxIdCustomer = new System.Windows.Forms.TextBox();
            this.lbIdCustommer = new System.Windows.Forms.Label();
            this.lbListCustomer = new System.Windows.Forms.Label();
            this.btnAddCustomer = new System.Windows.Forms.Button();
            this.btnSaveCustomer = new System.Windows.Forms.Button();
            this.btnDeleteCustomer = new System.Windows.Forms.Button();
            this.btnEditCustomer = new System.Windows.Forms.Button();
            this.btnExitCustomer = new System.Windows.Forms.Button();
            this.panel1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgvCustomer)).BeginInit();
            this.SuspendLayout();
            // 
            // panel1
            // 
            this.panel1.Controls.Add(this.dgvCustomer);
            this.panel1.Controls.Add(this.dtpCreate_atCustomer);
            this.panel1.Controls.Add(this.tbxAddressCustomer);
            this.panel1.Controls.Add(this.lbAddressCustomer);
            this.panel1.Controls.Add(this.lbCreate_atCustomer);
            this.panel1.Controls.Add(this.tbxNameCustomer);
            this.panel1.Controls.Add(this.lbNameCustomer);
            this.panel1.Controls.Add(this.tbxEmailCustomer);
            this.panel1.Controls.Add(this.lbEmailCustomer);
            this.panel1.Controls.Add(this.tbxPhoneCustomer);
            this.panel1.Controls.Add(this.lbPhoneCustomer);
            this.panel1.Controls.Add(this.tbxIdCustomer);
            this.panel1.Controls.Add(this.lbIdCustommer);
            this.panel1.Controls.Add(this.lbListCustomer);
            this.panel1.Location = new System.Drawing.Point(14, 16);
            this.panel1.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(1055, 595);
            this.panel1.TabIndex = 0;
            // 
            // dgvCustomer
            // 
            this.dgvCustomer.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgvCustomer.Location = new System.Drawing.Point(0, 320);
            this.dgvCustomer.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.dgvCustomer.Name = "dgvCustomer";
            this.dgvCustomer.RowHeadersWidth = 51;
            this.dgvCustomer.RowTemplate.Height = 25;
            this.dgvCustomer.Size = new System.Drawing.Size(1055, 261);
            this.dgvCustomer.TabIndex = 11;
            // 
            // dtpCreate_atCustomer
            // 
            this.dtpCreate_atCustomer.Location = new System.Drawing.Point(37, 263);
            this.dtpCreate_atCustomer.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.dtpCreate_atCustomer.Name = "dtpCreate_atCustomer";
            this.dtpCreate_atCustomer.Size = new System.Drawing.Size(294, 27);
            this.dtpCreate_atCustomer.TabIndex = 10;
            // 
            // tbxAddressCustomer
            // 
            this.tbxAddressCustomer.Location = new System.Drawing.Point(709, 264);
            this.tbxAddressCustomer.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.tbxAddressCustomer.Name = "tbxAddressCustomer";
            this.tbxAddressCustomer.Size = new System.Drawing.Size(268, 27);
            this.tbxAddressCustomer.TabIndex = 4;
            // 
            // lbAddressCustomer
            // 
            this.lbAddressCustomer.AutoSize = true;
            this.lbAddressCustomer.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(224)))), ((int)(((byte)(192)))));
            this.lbAddressCustomer.Font = new System.Drawing.Font("Segoe UI", 11F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point);
            this.lbAddressCustomer.Location = new System.Drawing.Point(709, 232);
            this.lbAddressCustomer.Name = "lbAddressCustomer";
            this.lbAddressCustomer.Padding = new System.Windows.Forms.Padding(0, 0, 67, 0);
            this.lbAddressCustomer.Size = new System.Drawing.Size(140, 25);
            this.lbAddressCustomer.TabIndex = 3;
            this.lbAddressCustomer.Text = "Địa Chỉ";
            // 
            // lbCreate_atCustomer
            // 
            this.lbCreate_atCustomer.AutoSize = true;
            this.lbCreate_atCustomer.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(224)))), ((int)(((byte)(192)))));
            this.lbCreate_atCustomer.Font = new System.Drawing.Font("Segoe UI", 11F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point);
            this.lbCreate_atCustomer.Location = new System.Drawing.Point(37, 232);
            this.lbCreate_atCustomer.Name = "lbCreate_atCustomer";
            this.lbCreate_atCustomer.Padding = new System.Windows.Forms.Padding(0, 0, 53, 0);
            this.lbCreate_atCustomer.Size = new System.Drawing.Size(145, 25);
            this.lbCreate_atCustomer.TabIndex = 9;
            this.lbCreate_atCustomer.Text = "Ngày Tạo";
            // 
            // tbxNameCustomer
            // 
            this.tbxNameCustomer.Location = new System.Drawing.Point(37, 177);
            this.tbxNameCustomer.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.tbxNameCustomer.Name = "tbxNameCustomer";
            this.tbxNameCustomer.Size = new System.Drawing.Size(294, 27);
            this.tbxNameCustomer.TabIndex = 8;
            // 
            // lbNameCustomer
            // 
            this.lbNameCustomer.AutoSize = true;
            this.lbNameCustomer.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(224)))), ((int)(((byte)(192)))));
            this.lbNameCustomer.Font = new System.Drawing.Font("Segoe UI", 11F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point);
            this.lbNameCustomer.Location = new System.Drawing.Point(37, 147);
            this.lbNameCustomer.Name = "lbNameCustomer";
            this.lbNameCustomer.Size = new System.Drawing.Size(148, 25);
            this.lbNameCustomer.TabIndex = 7;
            this.lbNameCustomer.Text = "Tên Khách Hàng";
            // 
            // tbxEmailCustomer
            // 
            this.tbxEmailCustomer.Location = new System.Drawing.Point(709, 99);
            this.tbxEmailCustomer.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.tbxEmailCustomer.Name = "tbxEmailCustomer";
            this.tbxEmailCustomer.Size = new System.Drawing.Size(268, 27);
            this.tbxEmailCustomer.TabIndex = 6;
            // 
            // lbEmailCustomer
            // 
            this.lbEmailCustomer.AutoSize = true;
            this.lbEmailCustomer.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(224)))), ((int)(((byte)(192)))));
            this.lbEmailCustomer.Font = new System.Drawing.Font("Segoe UI", 11F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point);
            this.lbEmailCustomer.Location = new System.Drawing.Point(709, 67);
            this.lbEmailCustomer.Name = "lbEmailCustomer";
            this.lbEmailCustomer.Padding = new System.Windows.Forms.Padding(0, 0, 80, 0);
            this.lbEmailCustomer.Size = new System.Drawing.Size(138, 25);
            this.lbEmailCustomer.TabIndex = 5;
            this.lbEmailCustomer.Text = "Email";
            // 
            // tbxPhoneCustomer
            // 
            this.tbxPhoneCustomer.Location = new System.Drawing.Point(709, 177);
            this.tbxPhoneCustomer.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.tbxPhoneCustomer.Name = "tbxPhoneCustomer";
            this.tbxPhoneCustomer.Size = new System.Drawing.Size(268, 27);
            this.tbxPhoneCustomer.TabIndex = 4;
            // 
            // lbPhoneCustomer
            // 
            this.lbPhoneCustomer.AutoSize = true;
            this.lbPhoneCustomer.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(224)))), ((int)(((byte)(192)))));
            this.lbPhoneCustomer.Font = new System.Drawing.Font("Segoe UI", 11F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point);
            this.lbPhoneCustomer.Location = new System.Drawing.Point(709, 147);
            this.lbPhoneCustomer.Name = "lbPhoneCustomer";
            this.lbPhoneCustomer.Padding = new System.Windows.Forms.Padding(0, 0, 16, 0);
            this.lbPhoneCustomer.Size = new System.Drawing.Size(145, 25);
            this.lbPhoneCustomer.TabIndex = 3;
            this.lbPhoneCustomer.Text = "Số Điện Thoại";
            // 
            // tbxIdCustomer
            // 
            this.tbxIdCustomer.Location = new System.Drawing.Point(37, 99);
            this.tbxIdCustomer.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.tbxIdCustomer.Name = "tbxIdCustomer";
            this.tbxIdCustomer.Size = new System.Drawing.Size(294, 27);
            this.tbxIdCustomer.TabIndex = 2;
            // 
            // lbIdCustommer
            // 
            this.lbIdCustommer.AutoSize = true;
            this.lbIdCustommer.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(224)))), ((int)(((byte)(192)))));
            this.lbIdCustommer.Font = new System.Drawing.Font("Segoe UI", 11F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point);
            this.lbIdCustommer.Location = new System.Drawing.Point(37, 67);
            this.lbIdCustommer.Name = "lbIdCustommer";
            this.lbIdCustommer.Size = new System.Drawing.Size(146, 25);
            this.lbIdCustommer.TabIndex = 1;
            this.lbIdCustommer.Text = "Mã Khách Hàng";
            // 
            // lbListCustomer
            // 
            this.lbListCustomer.AutoSize = true;
            this.lbListCustomer.Font = new System.Drawing.Font("Segoe UI", 20F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point);
            this.lbListCustomer.Location = new System.Drawing.Point(368, 0);
            this.lbListCustomer.Name = "lbListCustomer";
            this.lbListCustomer.Size = new System.Drawing.Size(386, 46);
            this.lbListCustomer.TabIndex = 0;
            this.lbListCustomer.Text = "Danh Sách Khách Hàng";
            this.lbListCustomer.Click += new System.EventHandler(this.label1_Click);
            // 
            // btnAddCustomer
            // 
            this.btnAddCustomer.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(192)))), ((int)(((byte)(128)))));
            this.btnAddCustomer.Location = new System.Drawing.Point(139, 619);
            this.btnAddCustomer.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.btnAddCustomer.Name = "btnAddCustomer";
            this.btnAddCustomer.Size = new System.Drawing.Size(117, 47);
            this.btnAddCustomer.TabIndex = 11;
            this.btnAddCustomer.Text = "Tạo Mới";
            this.btnAddCustomer.UseVisualStyleBackColor = false;
            this.btnAddCustomer.Click += new System.EventHandler(this.btnAddCustomer_Click);
            // 
            // btnSaveCustomer
            // 
            this.btnSaveCustomer.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(192)))), ((int)(((byte)(128)))));
            this.btnSaveCustomer.Location = new System.Drawing.Point(559, 619);
            this.btnSaveCustomer.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.btnSaveCustomer.Name = "btnSaveCustomer";
            this.btnSaveCustomer.Size = new System.Drawing.Size(114, 47);
            this.btnSaveCustomer.TabIndex = 12;
            this.btnSaveCustomer.Text = "Lưu";
            this.btnSaveCustomer.UseVisualStyleBackColor = false;
            // 
            // btnDeleteCustomer
            // 
            this.btnDeleteCustomer.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(192)))), ((int)(((byte)(128)))));
            this.btnDeleteCustomer.Location = new System.Drawing.Point(278, 619);
            this.btnDeleteCustomer.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.btnDeleteCustomer.Name = "btnDeleteCustomer";
            this.btnDeleteCustomer.Size = new System.Drawing.Size(112, 47);
            this.btnDeleteCustomer.TabIndex = 12;
            this.btnDeleteCustomer.Text = "Xóa";
            this.btnDeleteCustomer.UseVisualStyleBackColor = false;
            // 
            // btnEditCustomer
            // 
            this.btnEditCustomer.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(192)))), ((int)(((byte)(128)))));
            this.btnEditCustomer.Location = new System.Drawing.Point(413, 619);
            this.btnEditCustomer.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.btnEditCustomer.Name = "btnEditCustomer";
            this.btnEditCustomer.Size = new System.Drawing.Size(126, 47);
            this.btnEditCustomer.TabIndex = 13;
            this.btnEditCustomer.Text = "Sửa";
            this.btnEditCustomer.UseVisualStyleBackColor = false;
            // 
            // btnExitCustomer
            // 
            this.btnExitCustomer.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(192)))), ((int)(((byte)(128)))));
            this.btnExitCustomer.Location = new System.Drawing.Point(703, 619);
            this.btnExitCustomer.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.btnExitCustomer.Name = "btnExitCustomer";
            this.btnExitCustomer.Size = new System.Drawing.Size(115, 47);
            this.btnExitCustomer.TabIndex = 14;
            this.btnExitCustomer.Text = "Thoát";
            this.btnExitCustomer.UseVisualStyleBackColor = false;
            // 
            // frmCustomer
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 20F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1082, 681);
            this.Controls.Add(this.btnExitCustomer);
            this.Controls.Add(this.btnEditCustomer);
            this.Controls.Add(this.btnDeleteCustomer);
            this.Controls.Add(this.btnAddCustomer);
            this.Controls.Add(this.btnSaveCustomer);
            this.Controls.Add(this.panel1);
            this.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.Name = "frmCustomer";
            this.Text = "frmCustomer";
            this.Load += new System.EventHandler(this.frmCustomer_Load);
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgvCustomer)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Label lbListCustomer;
        private System.Windows.Forms.Button btnSaveCustomer;
        private System.Windows.Forms.Button btnAddCustomer;
        private System.Windows.Forms.DateTimePicker dtpCreate_atCustomer;
        private System.Windows.Forms.TextBox tbxAddressCustomer;
        private System.Windows.Forms.Label lbAddressCustomer;
        private System.Windows.Forms.Label lbCreate_atCustomer;
        private System.Windows.Forms.TextBox tbxNameCustomer;
        private System.Windows.Forms.Label lbNameCustomer;
        private System.Windows.Forms.TextBox tbxEmailCustomer;
        private System.Windows.Forms.Label lbEmailCustomer;
        private System.Windows.Forms.TextBox tbxPhoneCustomer;
        private System.Windows.Forms.Label lbPhoneCustomer;
        private System.Windows.Forms.TextBox tbxIdCustomer;
        private System.Windows.Forms.Label lbIdCustommer;
        private System.Windows.Forms.DataGridView dgvCustomer;
        private System.Windows.Forms.Button btnDeleteCustomer;
        private System.Windows.Forms.Button btnEditCustomer;
        private System.Windows.Forms.Button btnExitCustomer;
    }
}