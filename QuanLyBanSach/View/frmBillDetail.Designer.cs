﻿
namespace QuanLyBanSach.View
{
    partial class frmBillDetail
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frmBillDetail));
            this.btnImportAdd = new System.Windows.Forms.Button();
            this.gbxNameBook = new System.Windows.Forms.GroupBox();
            this.cbxNameBook = new System.Windows.Forms.ComboBox();
            this.flowLayoutPanel2 = new System.Windows.Forms.FlowLayoutPanel();
            this.gbxIdBook = new System.Windows.Forms.GroupBox();
            this.textBox4 = new System.Windows.Forms.TextBox();
            this.gbxCount = new System.Windows.Forms.GroupBox();
            this.textBox5 = new System.Windows.Forms.TextBox();
            this.gbxPrice = new System.Windows.Forms.GroupBox();
            this.textBox6 = new System.Windows.Forms.TextBox();
            this.btnSave = new System.Windows.Forms.Button();
            this.btnCancel = new System.Windows.Forms.Button();
            this.flowLayoutPanel1 = new System.Windows.Forms.FlowLayoutPanel();
            this.gbxIdBill = new System.Windows.Forms.GroupBox();
            this.comboBox1 = new System.Windows.Forms.ComboBox();
            this.gbxNameCustomer = new System.Windows.Forms.GroupBox();
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            this.comboBox2 = new System.Windows.Forms.ComboBox();
            this.gbxIdEmployee = new System.Windows.Forms.GroupBox();
            this.textBox3 = new System.Windows.Forms.TextBox();
            this.btnAdd = new System.Windows.Forms.Button();
            this.btnDelete = new System.Windows.Forms.Button();
            this.btnRepair = new System.Windows.Forms.Button();
            this.flowLayoutPanel3 = new System.Windows.Forms.FlowLayoutPanel();
            this.dataGridView1 = new System.Windows.Forms.DataGridView();
            this.btnExit = new System.Windows.Forms.Button();
            this.btnFind = new System.Windows.Forms.Button();
            this.gbxNameBook.SuspendLayout();
            this.flowLayoutPanel2.SuspendLayout();
            this.gbxIdBook.SuspendLayout();
            this.gbxCount.SuspendLayout();
            this.gbxPrice.SuspendLayout();
            this.flowLayoutPanel1.SuspendLayout();
            this.gbxIdBill.SuspendLayout();
            this.gbxNameCustomer.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            this.gbxIdEmployee.SuspendLayout();
            this.flowLayoutPanel3.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView1)).BeginInit();
            this.SuspendLayout();
            // 
            // btnImportAdd
            // 
            this.btnImportAdd.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(192)))), ((int)(((byte)(128)))));
            this.btnImportAdd.Location = new System.Drawing.Point(19, 92);
            this.btnImportAdd.Margin = new System.Windows.Forms.Padding(4, 3, 4, 3);
            this.btnImportAdd.Name = "btnImportAdd";
            this.btnImportAdd.Size = new System.Drawing.Size(88, 23);
            this.btnImportAdd.TabIndex = 18;
            this.btnImportAdd.Text = "Tạo mới";
            this.btnImportAdd.UseVisualStyleBackColor = false;
            // 
            // gbxNameBook
            // 
            this.gbxNameBook.Controls.Add(this.cbxNameBook);
            this.gbxNameBook.Location = new System.Drawing.Point(4, 57);
            this.gbxNameBook.Margin = new System.Windows.Forms.Padding(4, 3, 4, 3);
            this.gbxNameBook.Name = "gbxNameBook";
            this.gbxNameBook.Padding = new System.Windows.Forms.Padding(4, 3, 4, 3);
            this.gbxNameBook.Size = new System.Drawing.Size(233, 58);
            this.gbxNameBook.TabIndex = 6;
            this.gbxNameBook.TabStop = false;
            this.gbxNameBook.Text = "Tên Sách";
            // 
            // cbxNameBook
            // 
            this.cbxNameBook.FormattingEnabled = true;
            this.cbxNameBook.Location = new System.Drawing.Point(7, 22);
            this.cbxNameBook.Margin = new System.Windows.Forms.Padding(4, 3, 4, 3);
            this.cbxNameBook.Name = "cbxNameBook";
            this.cbxNameBook.Size = new System.Drawing.Size(219, 23);
            this.cbxNameBook.TabIndex = 0;
            // 
            // flowLayoutPanel2
            // 
            this.flowLayoutPanel2.Controls.Add(this.gbxIdBook);
            this.flowLayoutPanel2.Controls.Add(this.gbxNameBook);
            this.flowLayoutPanel2.Controls.Add(this.gbxCount);
            this.flowLayoutPanel2.Controls.Add(this.gbxPrice);
            this.flowLayoutPanel2.Controls.Add(this.btnSave);
            this.flowLayoutPanel2.Controls.Add(this.btnCancel);
            this.flowLayoutPanel2.Location = new System.Drawing.Point(652, 9);
            this.flowLayoutPanel2.Margin = new System.Windows.Forms.Padding(4, 3, 4, 3);
            this.flowLayoutPanel2.Name = "flowLayoutPanel2";
            this.flowLayoutPanel2.Size = new System.Drawing.Size(267, 468);
            this.flowLayoutPanel2.TabIndex = 15;
            // 
            // gbxIdBook
            // 
            this.gbxIdBook.Controls.Add(this.textBox4);
            this.gbxIdBook.Location = new System.Drawing.Point(4, 3);
            this.gbxIdBook.Margin = new System.Windows.Forms.Padding(4, 3, 4, 3);
            this.gbxIdBook.Name = "gbxIdBook";
            this.gbxIdBook.Padding = new System.Windows.Forms.Padding(4, 3, 4, 3);
            this.gbxIdBook.Size = new System.Drawing.Size(233, 48);
            this.gbxIdBook.TabIndex = 0;
            this.gbxIdBook.TabStop = false;
            this.gbxIdBook.Text = "Mã Sách";
            // 
            // textBox4
            // 
            this.textBox4.Location = new System.Drawing.Point(7, 18);
            this.textBox4.Margin = new System.Windows.Forms.Padding(4, 3, 4, 3);
            this.textBox4.Name = "textBox4";
            this.textBox4.Size = new System.Drawing.Size(219, 23);
            this.textBox4.TabIndex = 0;
            // 
            // gbxCount
            // 
            this.gbxCount.Controls.Add(this.textBox5);
            this.gbxCount.Location = new System.Drawing.Point(4, 121);
            this.gbxCount.Margin = new System.Windows.Forms.Padding(4, 3, 4, 3);
            this.gbxCount.Name = "gbxCount";
            this.gbxCount.Padding = new System.Windows.Forms.Padding(4, 3, 4, 3);
            this.gbxCount.Size = new System.Drawing.Size(233, 48);
            this.gbxCount.TabIndex = 1;
            this.gbxCount.TabStop = false;
            this.gbxCount.Text = "Số Lượng";
            // 
            // textBox5
            // 
            this.textBox5.Location = new System.Drawing.Point(7, 18);
            this.textBox5.Margin = new System.Windows.Forms.Padding(4, 3, 4, 3);
            this.textBox5.Name = "textBox5";
            this.textBox5.Size = new System.Drawing.Size(219, 23);
            this.textBox5.TabIndex = 0;
            // 
            // gbxPrice
            // 
            this.gbxPrice.BackColor = System.Drawing.SystemColors.Control;
            this.gbxPrice.Controls.Add(this.textBox6);
            this.gbxPrice.Location = new System.Drawing.Point(4, 175);
            this.gbxPrice.Margin = new System.Windows.Forms.Padding(4, 3, 4, 3);
            this.gbxPrice.Name = "gbxPrice";
            this.gbxPrice.Padding = new System.Windows.Forms.Padding(4, 3, 4, 3);
            this.gbxPrice.Size = new System.Drawing.Size(233, 48);
            this.gbxPrice.TabIndex = 2;
            this.gbxPrice.TabStop = false;
            this.gbxPrice.Text = "Đơn Giá";
            // 
            // textBox6
            // 
            this.textBox6.Location = new System.Drawing.Point(7, 18);
            this.textBox6.Margin = new System.Windows.Forms.Padding(4, 3, 4, 3);
            this.textBox6.Name = "textBox6";
            this.textBox6.Size = new System.Drawing.Size(219, 23);
            this.textBox6.TabIndex = 0;
            // 
            // btnSave
            // 
            this.btnSave.Location = new System.Drawing.Point(4, 229);
            this.btnSave.Margin = new System.Windows.Forms.Padding(4, 3, 4, 3);
            this.btnSave.Name = "btnSave";
            this.btnSave.Size = new System.Drawing.Size(88, 27);
            this.btnSave.TabIndex = 3;
            this.btnSave.Text = "Lưu";
            this.btnSave.UseVisualStyleBackColor = true;
            // 
            // btnCancel
            // 
            this.btnCancel.Location = new System.Drawing.Point(100, 229);
            this.btnCancel.Margin = new System.Windows.Forms.Padding(4, 3, 4, 3);
            this.btnCancel.Name = "btnCancel";
            this.btnCancel.Size = new System.Drawing.Size(88, 27);
            this.btnCancel.TabIndex = 4;
            this.btnCancel.Text = "Hủy";
            this.btnCancel.UseVisualStyleBackColor = true;
            // 
            // flowLayoutPanel1
            // 
            this.flowLayoutPanel1.Controls.Add(this.gbxIdBill);
            this.flowLayoutPanel1.Controls.Add(this.gbxNameCustomer);
            this.flowLayoutPanel1.Controls.Add(this.gbxIdEmployee);
            this.flowLayoutPanel1.Location = new System.Drawing.Point(15, 9);
            this.flowLayoutPanel1.Margin = new System.Windows.Forms.Padding(4, 3, 4, 3);
            this.flowLayoutPanel1.Name = "flowLayoutPanel1";
            this.flowLayoutPanel1.Size = new System.Drawing.Size(612, 76);
            this.flowLayoutPanel1.TabIndex = 14;
            // 
            // gbxIdBill
            // 
            this.gbxIdBill.BackColor = System.Drawing.Color.NavajoWhite;
            this.gbxIdBill.Controls.Add(this.comboBox1);
            this.gbxIdBill.Location = new System.Drawing.Point(4, 3);
            this.gbxIdBill.Margin = new System.Windows.Forms.Padding(4, 3, 4, 3);
            this.gbxIdBill.Name = "gbxIdBill";
            this.gbxIdBill.Padding = new System.Windows.Forms.Padding(4, 3, 4, 3);
            this.gbxIdBill.Size = new System.Drawing.Size(149, 53);
            this.gbxIdBill.TabIndex = 0;
            this.gbxIdBill.TabStop = false;
            this.gbxIdBill.Text = "Số Hóa Đơn";
            // 
            // comboBox1
            // 
            this.comboBox1.FormattingEnabled = true;
            this.comboBox1.Location = new System.Drawing.Point(7, 22);
            this.comboBox1.Margin = new System.Windows.Forms.Padding(4, 3, 4, 3);
            this.comboBox1.Name = "comboBox1";
            this.comboBox1.Size = new System.Drawing.Size(135, 23);
            this.comboBox1.TabIndex = 0;
            // 
            // gbxNameCustomer
            // 
            this.gbxNameCustomer.BackColor = System.Drawing.Color.NavajoWhite;
            this.gbxNameCustomer.Controls.Add(this.pictureBox1);
            this.gbxNameCustomer.Controls.Add(this.comboBox2);
            this.gbxNameCustomer.Location = new System.Drawing.Point(161, 3);
            this.gbxNameCustomer.Margin = new System.Windows.Forms.Padding(4, 3, 4, 3);
            this.gbxNameCustomer.Name = "gbxNameCustomer";
            this.gbxNameCustomer.Padding = new System.Windows.Forms.Padding(4, 3, 4, 3);
            this.gbxNameCustomer.Size = new System.Drawing.Size(197, 53);
            this.gbxNameCustomer.TabIndex = 1;
            this.gbxNameCustomer.TabStop = false;
            this.gbxNameCustomer.Text = "Tên Khách Hàng";
            // 
            // pictureBox1
            // 
            this.pictureBox1.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.pictureBox1.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("pictureBox1.BackgroundImage")));
            this.pictureBox1.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center;
            this.pictureBox1.Location = new System.Drawing.Point(175, 23);
            this.pictureBox1.Margin = new System.Windows.Forms.Padding(4, 3, 4, 3);
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new System.Drawing.Size(22, 24);
            this.pictureBox1.TabIndex = 1;
            this.pictureBox1.TabStop = false;
            this.pictureBox1.Click += new System.EventHandler(this.pictureBox1_Click);
            // 
            // comboBox2
            // 
            this.comboBox2.FormattingEnabled = true;
            this.comboBox2.Location = new System.Drawing.Point(7, 22);
            this.comboBox2.Margin = new System.Windows.Forms.Padding(4, 3, 4, 3);
            this.comboBox2.Name = "comboBox2";
            this.comboBox2.Size = new System.Drawing.Size(168, 23);
            this.comboBox2.TabIndex = 0;
            // 
            // gbxIdEmployee
            // 
            this.gbxIdEmployee.BackColor = System.Drawing.Color.NavajoWhite;
            this.gbxIdEmployee.Controls.Add(this.textBox3);
            this.gbxIdEmployee.Location = new System.Drawing.Point(366, 3);
            this.gbxIdEmployee.Margin = new System.Windows.Forms.Padding(4, 3, 4, 3);
            this.gbxIdEmployee.Name = "gbxIdEmployee";
            this.gbxIdEmployee.Padding = new System.Windows.Forms.Padding(4, 3, 4, 3);
            this.gbxIdEmployee.Size = new System.Drawing.Size(201, 53);
            this.gbxIdEmployee.TabIndex = 1;
            this.gbxIdEmployee.TabStop = false;
            this.gbxIdEmployee.Text = "Mã Nhân Viên";
            // 
            // textBox3
            // 
            this.textBox3.Location = new System.Drawing.Point(7, 22);
            this.textBox3.Margin = new System.Windows.Forms.Padding(4, 3, 4, 3);
            this.textBox3.Name = "textBox3";
            this.textBox3.Size = new System.Drawing.Size(165, 23);
            this.textBox3.TabIndex = 0;
            // 
            // btnAdd
            // 
            this.btnAdd.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(192)))), ((int)(((byte)(128)))));
            this.btnAdd.Location = new System.Drawing.Point(128, 254);
            this.btnAdd.Margin = new System.Windows.Forms.Padding(128, 3, 4, 3);
            this.btnAdd.Name = "btnAdd";
            this.btnAdd.Size = new System.Drawing.Size(78, 27);
            this.btnAdd.TabIndex = 3;
            this.btnAdd.Text = "Thêm";
            this.btnAdd.UseVisualStyleBackColor = false;
            // 
            // btnDelete
            // 
            this.btnDelete.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(192)))), ((int)(((byte)(128)))));
            this.btnDelete.Location = new System.Drawing.Point(214, 254);
            this.btnDelete.Margin = new System.Windows.Forms.Padding(4, 3, 4, 3);
            this.btnDelete.Name = "btnDelete";
            this.btnDelete.Size = new System.Drawing.Size(74, 27);
            this.btnDelete.TabIndex = 4;
            this.btnDelete.Text = "Xóa";
            this.btnDelete.UseVisualStyleBackColor = false;
            // 
            // btnRepair
            // 
            this.btnRepair.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(192)))), ((int)(((byte)(128)))));
            this.btnRepair.Location = new System.Drawing.Point(296, 254);
            this.btnRepair.Margin = new System.Windows.Forms.Padding(4, 3, 4, 3);
            this.btnRepair.Name = "btnRepair";
            this.btnRepair.Size = new System.Drawing.Size(83, 27);
            this.btnRepair.TabIndex = 5;
            this.btnRepair.Text = "Sửa";
            this.btnRepair.UseVisualStyleBackColor = false;
            // 
            // flowLayoutPanel3
            // 
            this.flowLayoutPanel3.Controls.Add(this.dataGridView1);
            this.flowLayoutPanel3.Controls.Add(this.btnAdd);
            this.flowLayoutPanel3.Controls.Add(this.btnDelete);
            this.flowLayoutPanel3.Controls.Add(this.btnRepair);
            this.flowLayoutPanel3.Controls.Add(this.btnExit);
            this.flowLayoutPanel3.Location = new System.Drawing.Point(14, 142);
            this.flowLayoutPanel3.Margin = new System.Windows.Forms.Padding(4, 3, 4, 3);
            this.flowLayoutPanel3.Name = "flowLayoutPanel3";
            this.flowLayoutPanel3.Size = new System.Drawing.Size(614, 336);
            this.flowLayoutPanel3.TabIndex = 16;
            // 
            // dataGridView1
            // 
            this.dataGridView1.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridView1.Location = new System.Drawing.Point(4, 3);
            this.dataGridView1.Margin = new System.Windows.Forms.Padding(4, 3, 4, 3);
            this.dataGridView1.Name = "dataGridView1";
            this.dataGridView1.Size = new System.Drawing.Size(610, 245);
            this.dataGridView1.TabIndex = 6;
            // 
            // btnExit
            // 
            this.btnExit.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(192)))), ((int)(((byte)(128)))));
            this.btnExit.Location = new System.Drawing.Point(387, 254);
            this.btnExit.Margin = new System.Windows.Forms.Padding(4, 3, 4, 3);
            this.btnExit.Name = "btnExit";
            this.btnExit.Size = new System.Drawing.Size(82, 27);
            this.btnExit.TabIndex = 8;
            this.btnExit.Text = "Thoát";
            this.btnExit.UseVisualStyleBackColor = false;
            // 
            // btnFind
            // 
            this.btnFind.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(192)))), ((int)(((byte)(128)))));
            this.btnFind.Location = new System.Drawing.Point(115, 92);
            this.btnFind.Margin = new System.Windows.Forms.Padding(4, 3, 4, 3);
            this.btnFind.Name = "btnFind";
            this.btnFind.Size = new System.Drawing.Size(88, 23);
            this.btnFind.TabIndex = 17;
            this.btnFind.Text = "Tìm Kiếm";
            this.btnFind.UseVisualStyleBackColor = false;
            // 
            // frmBillDetail
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 15F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(933, 487);
            this.Controls.Add(this.btnImportAdd);
            this.Controls.Add(this.flowLayoutPanel2);
            this.Controls.Add(this.flowLayoutPanel1);
            this.Controls.Add(this.flowLayoutPanel3);
            this.Controls.Add(this.btnFind);
            this.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.Name = "frmBillDetail";
            this.Text = "Hóa Đơn";
            this.gbxNameBook.ResumeLayout(false);
            this.flowLayoutPanel2.ResumeLayout(false);
            this.gbxIdBook.ResumeLayout(false);
            this.gbxIdBook.PerformLayout();
            this.gbxCount.ResumeLayout(false);
            this.gbxCount.PerformLayout();
            this.gbxPrice.ResumeLayout(false);
            this.gbxPrice.PerformLayout();
            this.flowLayoutPanel1.ResumeLayout(false);
            this.gbxIdBill.ResumeLayout(false);
            this.gbxNameCustomer.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            this.gbxIdEmployee.ResumeLayout(false);
            this.gbxIdEmployee.PerformLayout();
            this.flowLayoutPanel3.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView1)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Button btnImportAdd;
        private System.Windows.Forms.GroupBox gbxNameBook;
        private System.Windows.Forms.ComboBox cbxNameBook;
        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel2;
        private System.Windows.Forms.GroupBox gbxIdBook;
        private System.Windows.Forms.TextBox textBox4;
        private System.Windows.Forms.GroupBox gbxCount;
        private System.Windows.Forms.TextBox textBox5;
        private System.Windows.Forms.GroupBox gbxPrice;
        private System.Windows.Forms.TextBox textBox6;
        private System.Windows.Forms.Button btnSave;
        private System.Windows.Forms.Button btnCancel;
        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel1;
        private System.Windows.Forms.GroupBox gbxIdBill;
        private System.Windows.Forms.ComboBox comboBox1;
        private System.Windows.Forms.GroupBox gbxNameCustomer;
        private System.Windows.Forms.PictureBox pictureBox1;
        private System.Windows.Forms.ComboBox comboBox2;
        private System.Windows.Forms.GroupBox gbxIdEmployee;
        private System.Windows.Forms.TextBox textBox3;
        private System.Windows.Forms.Button btnAdd;
        private System.Windows.Forms.Button btnDelete;
        private System.Windows.Forms.Button btnRepair;
        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel3;
        private System.Windows.Forms.DataGridView dataGridView1;
        private System.Windows.Forms.Button btnExit;
        private System.Windows.Forms.Button btnFind;
    }
}