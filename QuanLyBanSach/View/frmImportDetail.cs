﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;

namespace QuanLyBanSach.View
{
    public partial class frmImportDetail : Form
    {
        public frmImportDetail()
        {
            InitializeComponent();
        }

        private void ptbxSuplier_Click(object sender, EventArgs e)
        {
            View.frmSuplier frmSuplier = new View.frmSuplier();
            frmSuplier.Show();
        }
    }
}
