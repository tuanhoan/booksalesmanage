﻿using QuanLyBanSach.Models;
using QuanLyBanSach.Services;
using System;
using System.Data;
using System.Linq;
using System.Windows.Forms;

namespace QuanLyBanSach.View
{
    public partial class frmBook : Form
    {
        int CellClickId;
        public frmBook()
        {
            InitializeComponent();
            LoadData();
            LoadBook();
        }
        private void LoadBook()
        {
            dtgvBook.DataSource = BookServices.Instance.GetAll().Select(x =>
                            new
                            {
                                ID = x.Id,
                                Name = x.Name,
                                PublisherName = x.PublisherNavigation.Name,
                                AuthourName = x.AuthorNavigation.Name,
                                CategoryName = x.CategoryNavigation.Name,
                                Price = x.Price,
                                Count = x.Count
                            }).ToList();
            dtgvBook.Columns[0].HeaderText = "Mã Sách";
            dtgvBook.Columns[1].HeaderText = "Tên Sách";
            dtgvBook.Columns[2].HeaderText = "Nhà Xuất Bản";
            dtgvBook.Columns[3].HeaderText = "Tác Giả";
            dtgvBook.Columns[4].HeaderText = "Thể Loại";
            dtgvBook.Columns[6].HeaderText = "Số Lượng";
            dtgvBook.Columns[5].HeaderText = "Giá";
        }



        private void btnAdd_Click(object sender, EventArgs e)
        {
            Book book = new Book();
            book.Count = Convert.ToInt32(txbCount.Text);
            book.Name = txbName.Text;
            book.Price = Convert.ToInt32(txbPrice.Text);
            book.AuthorId = Convert.ToInt32(cbAuthor.SelectedValue);
            book.CategoryId = Convert.ToInt32(cbCategory.SelectedValue);
            book.PublisherId = Convert.ToInt32(cbPublisher.SelectedValue);
            BookServices.Instance.Insert(book);
            LoadBook();
        }
        private void LoadData()
        {
            cbAuthor.DataSource = AuthorServices.Instance.GetAll().ToList();
            cbAuthor.DisplayMember = "Name";
            cbAuthor.ValueMember = "Id";
            cbPublisher.DataSource = PublisherServices.Instance.GetAll().ToList();
            cbPublisher.DisplayMember = "Name";
            cbPublisher.ValueMember = "Id";
            cbCategory.DataSource = CategoryServices.Instance.GetAll().ToList();
            cbCategory.DisplayMember = "Name";
            cbCategory.ValueMember = "Id";
        }
        private void frmBook_Load(object sender, EventArgs e)
        {

        }

        private void dtgvBook_CellClick(object sender, DataGridViewCellEventArgs e)
        { 
            int a = e.RowIndex;
            CellClickId =Convert.ToInt32(dtgvBook.Rows[a].Cells[0].Value);
            txbName.Text = dtgvBook.Rows[a].Cells[1].Value.ToString();
            txbCount.Text = dtgvBook.Rows[a].Cells[6].Value.ToString();
            txbPrice.Text = dtgvBook.Rows[a].Cells[5].Value.ToString();
            cbAuthor.Text = dtgvBook.Rows[a].Cells[3].Value.ToString();
            cbCategory.Text = dtgvBook.Rows[a].Cells[4].Value.ToString();
            cbPublisher.Text = dtgvBook.Rows[a].Cells[2].Value.ToString();
        }

        private void btnEdit_Click(object sender, EventArgs e)
        {
            Book book = new Book()
            {
                Id = CellClickId,
                Count = Convert.ToInt32(txbCount.Text),
                Name = txbName.Text,
                Price = Convert.ToInt32(txbPrice.Text),
                AuthorId = Convert.ToInt32(cbAuthor.SelectedValue),
                CategoryId = Convert.ToInt32(cbCategory.SelectedValue),
                PublisherId = Convert.ToInt32(cbPublisher.SelectedValue)
            };
            BookServices.Instance.Update(book);
            LoadBook();
        }

        private void btnDelete_Click(object sender, EventArgs e)
        {
            Book book = new Book()
            {
                Id = CellClickId
            };
            BookServices.Instance.Delete(book);
            LoadBook();
        }

    }
}
