﻿
namespace QuanLyBanSach.View
{
    partial class frmImportDetail
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frmImportDetail));
            this.flowLayoutPanel3 = new System.Windows.Forms.FlowLayoutPanel();
            this.dataGridView1 = new System.Windows.Forms.DataGridView();
            this.btnAdd = new System.Windows.Forms.Button();
            this.btnDelete = new System.Windows.Forms.Button();
            this.btnExit = new System.Windows.Forms.Button();
            this.textBox6 = new System.Windows.Forms.TextBox();
            this.textBox5 = new System.Windows.Forms.TextBox();
            this.cbxNameBook = new System.Windows.Forms.ComboBox();
            this.textBox4 = new System.Windows.Forms.TextBox();
            this.gbxIdBook = new System.Windows.Forms.GroupBox();
            this.gbxNameBook = new System.Windows.Forms.GroupBox();
            this.btnCreateBill = new System.Windows.Forms.Button();
            this.btnFind = new System.Windows.Forms.Button();
            this.textBox3 = new System.Windows.Forms.TextBox();
            this.gbxCount = new System.Windows.Forms.GroupBox();
            this.gbxIdEmployee = new System.Windows.Forms.GroupBox();
            this.gbxPrice = new System.Windows.Forms.GroupBox();
            this.cbxNameSupplier = new System.Windows.Forms.ComboBox();
            this.btnSave = new System.Windows.Forms.Button();
            this.btnCancel = new System.Windows.Forms.Button();
            this.gbxNameSupplier = new System.Windows.Forms.GroupBox();
            this.ptbxSuplier = new System.Windows.Forms.PictureBox();
            this.cbxIdImport = new System.Windows.Forms.ComboBox();
            this.gbxIdImport = new System.Windows.Forms.GroupBox();
            this.flowLayoutPanel2 = new System.Windows.Forms.FlowLayoutPanel();
            this.flowLayoutPanel1 = new System.Windows.Forms.FlowLayoutPanel();
            this.flowLayoutPanel3.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView1)).BeginInit();
            this.gbxIdBook.SuspendLayout();
            this.gbxNameBook.SuspendLayout();
            this.gbxCount.SuspendLayout();
            this.gbxIdEmployee.SuspendLayout();
            this.gbxPrice.SuspendLayout();
            this.gbxNameSupplier.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.ptbxSuplier)).BeginInit();
            this.gbxIdImport.SuspendLayout();
            this.flowLayoutPanel2.SuspendLayout();
            this.flowLayoutPanel1.SuspendLayout();
            this.SuspendLayout();
            // 
            // flowLayoutPanel3
            // 
            this.flowLayoutPanel3.Controls.Add(this.dataGridView1);
            this.flowLayoutPanel3.Controls.Add(this.btnAdd);
            this.flowLayoutPanel3.Controls.Add(this.btnDelete);
            this.flowLayoutPanel3.Location = new System.Drawing.Point(25, 205);
            this.flowLayoutPanel3.Margin = new System.Windows.Forms.Padding(5, 4, 5, 4);
            this.flowLayoutPanel3.Name = "flowLayoutPanel3";
            this.flowLayoutPanel3.Size = new System.Drawing.Size(702, 407);
            this.flowLayoutPanel3.TabIndex = 11;
            // 
            // dataGridView1
            // 
            this.dataGridView1.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridView1.Location = new System.Drawing.Point(5, 4);
            this.dataGridView1.Margin = new System.Windows.Forms.Padding(5, 4, 5, 4);
            this.dataGridView1.Name = "dataGridView1";
            this.dataGridView1.RowHeadersWidth = 51;
            this.dataGridView1.Size = new System.Drawing.Size(697, 327);
            this.dataGridView1.TabIndex = 6;
            // 
            // btnAdd
            // 
            this.btnAdd.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(192)))), ((int)(((byte)(128)))));
            this.btnAdd.Location = new System.Drawing.Point(266, 339);
            this.btnAdd.Margin = new System.Windows.Forms.Padding(266, 4, 5, 4);
            this.btnAdd.Name = "btnAdd";
            this.btnAdd.Size = new System.Drawing.Size(89, 36);
            this.btnAdd.TabIndex = 3;
            this.btnAdd.Text = "Thêm";
            this.btnAdd.UseVisualStyleBackColor = false;
            // 
            // btnDelete
            // 
            this.btnDelete.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(192)))), ((int)(((byte)(128)))));
            this.btnDelete.Location = new System.Drawing.Point(365, 339);
            this.btnDelete.Margin = new System.Windows.Forms.Padding(5, 4, 5, 4);
            this.btnDelete.Name = "btnDelete";
            this.btnDelete.Size = new System.Drawing.Size(85, 36);
            this.btnDelete.TabIndex = 4;
            this.btnDelete.Text = "Xóa";
            this.btnDelete.UseVisualStyleBackColor = false;
            // 
            // btnExit
            // 
            this.btnExit.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(192)))), ((int)(((byte)(128)))));
            this.btnExit.Location = new System.Drawing.Point(967, 620);
            this.btnExit.Margin = new System.Windows.Forms.Padding(5, 4, 5, 4);
            this.btnExit.Name = "btnExit";
            this.btnExit.Size = new System.Drawing.Size(94, 36);
            this.btnExit.TabIndex = 12;
            this.btnExit.Text = "Thoát";
            this.btnExit.UseVisualStyleBackColor = false;
            // 
            // textBox6
            // 
            this.textBox6.Location = new System.Drawing.Point(8, 24);
            this.textBox6.Margin = new System.Windows.Forms.Padding(5, 4, 5, 4);
            this.textBox6.Name = "textBox6";
            this.textBox6.Size = new System.Drawing.Size(250, 27);
            this.textBox6.TabIndex = 0;
            // 
            // textBox5
            // 
            this.textBox5.Location = new System.Drawing.Point(8, 24);
            this.textBox5.Margin = new System.Windows.Forms.Padding(5, 4, 5, 4);
            this.textBox5.Name = "textBox5";
            this.textBox5.Size = new System.Drawing.Size(250, 27);
            this.textBox5.TabIndex = 0;
            // 
            // cbxNameBook
            // 
            this.cbxNameBook.FormattingEnabled = true;
            this.cbxNameBook.Location = new System.Drawing.Point(8, 29);
            this.cbxNameBook.Margin = new System.Windows.Forms.Padding(5, 4, 5, 4);
            this.cbxNameBook.Name = "cbxNameBook";
            this.cbxNameBook.Size = new System.Drawing.Size(250, 28);
            this.cbxNameBook.TabIndex = 0;
            // 
            // textBox4
            // 
            this.textBox4.Location = new System.Drawing.Point(8, 24);
            this.textBox4.Margin = new System.Windows.Forms.Padding(5, 4, 5, 4);
            this.textBox4.Name = "textBox4";
            this.textBox4.Size = new System.Drawing.Size(250, 27);
            this.textBox4.TabIndex = 0;
            // 
            // gbxIdBook
            // 
            this.gbxIdBook.Controls.Add(this.textBox4);
            this.gbxIdBook.Location = new System.Drawing.Point(5, 4);
            this.gbxIdBook.Margin = new System.Windows.Forms.Padding(5, 4, 5, 4);
            this.gbxIdBook.Name = "gbxIdBook";
            this.gbxIdBook.Padding = new System.Windows.Forms.Padding(5, 4, 5, 4);
            this.gbxIdBook.Size = new System.Drawing.Size(266, 64);
            this.gbxIdBook.TabIndex = 0;
            this.gbxIdBook.TabStop = false;
            this.gbxIdBook.Text = "Mã Sách";
            // 
            // gbxNameBook
            // 
            this.gbxNameBook.Controls.Add(this.cbxNameBook);
            this.gbxNameBook.Location = new System.Drawing.Point(5, 76);
            this.gbxNameBook.Margin = new System.Windows.Forms.Padding(5, 4, 5, 4);
            this.gbxNameBook.Name = "gbxNameBook";
            this.gbxNameBook.Padding = new System.Windows.Forms.Padding(5, 4, 5, 4);
            this.gbxNameBook.Size = new System.Drawing.Size(266, 77);
            this.gbxNameBook.TabIndex = 5;
            this.gbxNameBook.TabStop = false;
            this.gbxNameBook.Text = "Tên Sách";
            // 
            // btnCreateBill
            // 
            this.btnCreateBill.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(192)))), ((int)(((byte)(128)))));
            this.btnCreateBill.Location = new System.Drawing.Point(5, 83);
            this.btnCreateBill.Margin = new System.Windows.Forms.Padding(5, 4, 5, 4);
            this.btnCreateBill.Name = "btnCreateBill";
            this.btnCreateBill.Size = new System.Drawing.Size(101, 40);
            this.btnCreateBill.TabIndex = 11;
            this.btnCreateBill.Text = "Tạo mới";
            this.btnCreateBill.UseVisualStyleBackColor = false;
            // 
            // btnFind
            // 
            this.btnFind.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(192)))), ((int)(((byte)(128)))));
            this.btnFind.Location = new System.Drawing.Point(116, 83);
            this.btnFind.Margin = new System.Windows.Forms.Padding(5, 4, 5, 4);
            this.btnFind.Name = "btnFind";
            this.btnFind.Size = new System.Drawing.Size(101, 40);
            this.btnFind.TabIndex = 10;
            this.btnFind.Text = "Tìm Kiếm";
            this.btnFind.UseVisualStyleBackColor = false;
            // 
            // textBox3
            // 
            this.textBox3.Location = new System.Drawing.Point(8, 29);
            this.textBox3.Margin = new System.Windows.Forms.Padding(5, 4, 5, 4);
            this.textBox3.Name = "textBox3";
            this.textBox3.Size = new System.Drawing.Size(188, 27);
            this.textBox3.TabIndex = 0;
            // 
            // gbxCount
            // 
            this.gbxCount.Controls.Add(this.textBox5);
            this.gbxCount.Location = new System.Drawing.Point(5, 161);
            this.gbxCount.Margin = new System.Windows.Forms.Padding(5, 4, 5, 4);
            this.gbxCount.Name = "gbxCount";
            this.gbxCount.Padding = new System.Windows.Forms.Padding(5, 4, 5, 4);
            this.gbxCount.Size = new System.Drawing.Size(266, 64);
            this.gbxCount.TabIndex = 1;
            this.gbxCount.TabStop = false;
            this.gbxCount.Text = "Số Lượng";
            // 
            // gbxIdEmployee
            // 
            this.gbxIdEmployee.BackColor = System.Drawing.Color.NavajoWhite;
            this.gbxIdEmployee.Controls.Add(this.textBox3);
            this.gbxIdEmployee.Location = new System.Drawing.Point(420, 4);
            this.gbxIdEmployee.Margin = new System.Windows.Forms.Padding(5, 4, 5, 4);
            this.gbxIdEmployee.Name = "gbxIdEmployee";
            this.gbxIdEmployee.Padding = new System.Windows.Forms.Padding(5, 4, 5, 4);
            this.gbxIdEmployee.Size = new System.Drawing.Size(230, 71);
            this.gbxIdEmployee.TabIndex = 1;
            this.gbxIdEmployee.TabStop = false;
            this.gbxIdEmployee.Text = "Mã Nhân Viên";
            // 
            // gbxPrice
            // 
            this.gbxPrice.BackColor = System.Drawing.SystemColors.Control;
            this.gbxPrice.Controls.Add(this.textBox6);
            this.gbxPrice.Location = new System.Drawing.Point(5, 233);
            this.gbxPrice.Margin = new System.Windows.Forms.Padding(5, 4, 5, 4);
            this.gbxPrice.Name = "gbxPrice";
            this.gbxPrice.Padding = new System.Windows.Forms.Padding(5, 4, 5, 4);
            this.gbxPrice.Size = new System.Drawing.Size(266, 64);
            this.gbxPrice.TabIndex = 2;
            this.gbxPrice.TabStop = false;
            this.gbxPrice.Text = "Giá  Nhập";
            // 
            // cbxNameSupplier
            // 
            this.cbxNameSupplier.FormattingEnabled = true;
            this.cbxNameSupplier.Location = new System.Drawing.Point(9, 29);
            this.cbxNameSupplier.Margin = new System.Windows.Forms.Padding(5, 4, 5, 4);
            this.cbxNameSupplier.Name = "cbxNameSupplier";
            this.cbxNameSupplier.Size = new System.Drawing.Size(171, 28);
            this.cbxNameSupplier.TabIndex = 0;
            // 
            // btnSave
            // 
            this.btnSave.Location = new System.Drawing.Point(5, 305);
            this.btnSave.Margin = new System.Windows.Forms.Padding(5, 4, 5, 4);
            this.btnSave.Name = "btnSave";
            this.btnSave.Size = new System.Drawing.Size(101, 36);
            this.btnSave.TabIndex = 3;
            this.btnSave.Text = "Lưu";
            this.btnSave.UseVisualStyleBackColor = true;
            // 
            // btnCancel
            // 
            this.btnCancel.Location = new System.Drawing.Point(116, 305);
            this.btnCancel.Margin = new System.Windows.Forms.Padding(5, 4, 5, 4);
            this.btnCancel.Name = "btnCancel";
            this.btnCancel.Size = new System.Drawing.Size(101, 36);
            this.btnCancel.TabIndex = 4;
            this.btnCancel.Text = "Hủy";
            this.btnCancel.UseVisualStyleBackColor = true;
            // 
            // gbxNameSupplier
            // 
            this.gbxNameSupplier.BackColor = System.Drawing.Color.NavajoWhite;
            this.gbxNameSupplier.Controls.Add(this.ptbxSuplier);
            this.gbxNameSupplier.Controls.Add(this.cbxNameSupplier);
            this.gbxNameSupplier.Location = new System.Drawing.Point(185, 4);
            this.gbxNameSupplier.Margin = new System.Windows.Forms.Padding(5, 4, 5, 4);
            this.gbxNameSupplier.Name = "gbxNameSupplier";
            this.gbxNameSupplier.Padding = new System.Windows.Forms.Padding(5, 4, 5, 4);
            this.gbxNameSupplier.Size = new System.Drawing.Size(225, 71);
            this.gbxNameSupplier.TabIndex = 1;
            this.gbxNameSupplier.TabStop = false;
            this.gbxNameSupplier.Text = " Nhà Cung Cấp";
            // 
            // ptbxSuplier
            // 
            this.ptbxSuplier.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("ptbxSuplier.BackgroundImage")));
            this.ptbxSuplier.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center;
            this.ptbxSuplier.Location = new System.Drawing.Point(189, 27);
            this.ptbxSuplier.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.ptbxSuplier.Name = "ptbxSuplier";
            this.ptbxSuplier.Size = new System.Drawing.Size(29, 33);
            this.ptbxSuplier.TabIndex = 1;
            this.ptbxSuplier.TabStop = false;
            this.ptbxSuplier.Click += new System.EventHandler(this.ptbxSuplier_Click);
            // 
            // cbxIdImport
            // 
            this.cbxIdImport.FormattingEnabled = true;
            this.cbxIdImport.Location = new System.Drawing.Point(9, 29);
            this.cbxIdImport.Margin = new System.Windows.Forms.Padding(5, 4, 5, 4);
            this.cbxIdImport.Name = "cbxIdImport";
            this.cbxIdImport.Size = new System.Drawing.Size(159, 28);
            this.cbxIdImport.TabIndex = 0;
            // 
            // gbxIdImport
            // 
            this.gbxIdImport.BackColor = System.Drawing.Color.NavajoWhite;
            this.gbxIdImport.Controls.Add(this.cbxIdImport);
            this.gbxIdImport.Location = new System.Drawing.Point(5, 4);
            this.gbxIdImport.Margin = new System.Windows.Forms.Padding(5, 4, 5, 4);
            this.gbxIdImport.Name = "gbxIdImport";
            this.gbxIdImport.Padding = new System.Windows.Forms.Padding(5, 4, 5, 4);
            this.gbxIdImport.Size = new System.Drawing.Size(170, 71);
            this.gbxIdImport.TabIndex = 0;
            this.gbxIdImport.TabStop = false;
            this.gbxIdImport.Text = "Số Hóa Đơn Nhập";
            // 
            // flowLayoutPanel2
            // 
            this.flowLayoutPanel2.Controls.Add(this.gbxIdBook);
            this.flowLayoutPanel2.Controls.Add(this.gbxNameBook);
            this.flowLayoutPanel2.Controls.Add(this.gbxCount);
            this.flowLayoutPanel2.Controls.Add(this.gbxPrice);
            this.flowLayoutPanel2.Controls.Add(this.btnSave);
            this.flowLayoutPanel2.Controls.Add(this.btnCancel);
            this.flowLayoutPanel2.Location = new System.Drawing.Point(754, 28);
            this.flowLayoutPanel2.Margin = new System.Windows.Forms.Padding(5, 4, 5, 4);
            this.flowLayoutPanel2.Name = "flowLayoutPanel2";
            this.flowLayoutPanel2.Size = new System.Drawing.Size(305, 583);
            this.flowLayoutPanel2.TabIndex = 10;
            // 
            // flowLayoutPanel1
            // 
            this.flowLayoutPanel1.Controls.Add(this.gbxIdImport);
            this.flowLayoutPanel1.Controls.Add(this.gbxNameSupplier);
            this.flowLayoutPanel1.Controls.Add(this.gbxIdEmployee);
            this.flowLayoutPanel1.Controls.Add(this.btnCreateBill);
            this.flowLayoutPanel1.Controls.Add(this.btnFind);
            this.flowLayoutPanel1.Location = new System.Drawing.Point(26, 28);
            this.flowLayoutPanel1.Margin = new System.Windows.Forms.Padding(5, 4, 5, 4);
            this.flowLayoutPanel1.Name = "flowLayoutPanel1";
            this.flowLayoutPanel1.Size = new System.Drawing.Size(699, 140);
            this.flowLayoutPanel1.TabIndex = 9;
            // 
            // frmImportDetail
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 20F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1087, 683);
            this.Controls.Add(this.flowLayoutPanel3);
            this.Controls.Add(this.btnExit);
            this.Controls.Add(this.flowLayoutPanel2);
            this.Controls.Add(this.flowLayoutPanel1);
            this.Name = "frmImportDetail";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Phiếu nhập";
            this.flowLayoutPanel3.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView1)).EndInit();
            this.gbxIdBook.ResumeLayout(false);
            this.gbxIdBook.PerformLayout();
            this.gbxNameBook.ResumeLayout(false);
            this.gbxCount.ResumeLayout(false);
            this.gbxCount.PerformLayout();
            this.gbxIdEmployee.ResumeLayout(false);
            this.gbxIdEmployee.PerformLayout();
            this.gbxPrice.ResumeLayout(false);
            this.gbxPrice.PerformLayout();
            this.gbxNameSupplier.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.ptbxSuplier)).EndInit();
            this.gbxIdImport.ResumeLayout(false);
            this.flowLayoutPanel2.ResumeLayout(false);
            this.flowLayoutPanel1.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel3;
        private System.Windows.Forms.DataGridView dataGridView1;
        private System.Windows.Forms.Button btnAdd;
        private System.Windows.Forms.Button btnDelete;
        private System.Windows.Forms.Button btnExit;
        private System.Windows.Forms.TextBox textBox6;
        private System.Windows.Forms.TextBox textBox5;
        private System.Windows.Forms.ComboBox cbxNameBook;
        private System.Windows.Forms.TextBox textBox4;
        private System.Windows.Forms.GroupBox gbxIdBook;
        private System.Windows.Forms.GroupBox gbxNameBook;
        private System.Windows.Forms.Button btnCreateBill;
        private System.Windows.Forms.Button btnFind;
        private System.Windows.Forms.TextBox textBox3;
        private System.Windows.Forms.GroupBox gbxCount;
        private System.Windows.Forms.GroupBox gbxIdEmployee;
        private System.Windows.Forms.GroupBox gbxPrice;
        private System.Windows.Forms.ComboBox cbxNameSupplier;
        private System.Windows.Forms.Button btnSave;
        private System.Windows.Forms.Button btnCancel;
        private System.Windows.Forms.GroupBox gbxNameSupplier;
        private System.Windows.Forms.ComboBox cbxIdImport;
        private System.Windows.Forms.GroupBox gbxIdImport;
        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel2;
        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel1;
        private System.Windows.Forms.PictureBox ptbxSuplier;
    }
}