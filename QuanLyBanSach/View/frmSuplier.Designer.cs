﻿
namespace QuanLyBanSach.View
{
    partial class frmSuplier
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.gbxheadSuplier = new System.Windows.Forms.GroupBox();
            this.dtpCreate_atSuplier = new System.Windows.Forms.DateTimePicker();
            this.tbxAddressSuplier = new System.Windows.Forms.TextBox();
            this.lbAddressSuplier = new System.Windows.Forms.Label();
            this.lbCreate_atSuplier = new System.Windows.Forms.Label();
            this.tbxNameSuplier = new System.Windows.Forms.TextBox();
            this.lbNameSuplier = new System.Windows.Forms.Label();
            this.tbxEmailSuplier = new System.Windows.Forms.TextBox();
            this.lbEmailSuplier = new System.Windows.Forms.Label();
            this.tbxPhoneSuplier = new System.Windows.Forms.TextBox();
            this.lbPhoneSuplier = new System.Windows.Forms.Label();
            this.tbxIdSuplier = new System.Windows.Forms.TextBox();
            this.lbIdSuplier = new System.Windows.Forms.Label();
            this.panel1 = new System.Windows.Forms.Panel();
            this.lbSuplier = new System.Windows.Forms.Label();
            this.dgvSuplier = new System.Windows.Forms.DataGridView();
            this.btnExitSuplier = new System.Windows.Forms.Button();
            this.btnEditSuplier = new System.Windows.Forms.Button();
            this.btnDeleteSuplier = new System.Windows.Forms.Button();
            this.btnAddSuplier = new System.Windows.Forms.Button();
            this.btnSaveSuplier = new System.Windows.Forms.Button();
            this.gbxheadSuplier.SuspendLayout();
            this.panel1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgvSuplier)).BeginInit();
            this.SuspendLayout();
            // 
            // gbxheadSuplier
            // 
            this.gbxheadSuplier.Controls.Add(this.dtpCreate_atSuplier);
            this.gbxheadSuplier.Controls.Add(this.tbxAddressSuplier);
            this.gbxheadSuplier.Controls.Add(this.lbAddressSuplier);
            this.gbxheadSuplier.Controls.Add(this.lbCreate_atSuplier);
            this.gbxheadSuplier.Controls.Add(this.tbxNameSuplier);
            this.gbxheadSuplier.Controls.Add(this.lbNameSuplier);
            this.gbxheadSuplier.Controls.Add(this.tbxEmailSuplier);
            this.gbxheadSuplier.Controls.Add(this.lbEmailSuplier);
            this.gbxheadSuplier.Controls.Add(this.tbxPhoneSuplier);
            this.gbxheadSuplier.Controls.Add(this.lbPhoneSuplier);
            this.gbxheadSuplier.Controls.Add(this.tbxIdSuplier);
            this.gbxheadSuplier.Controls.Add(this.lbIdSuplier);
            this.gbxheadSuplier.Font = new System.Drawing.Font("Segoe UI", 11F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point);
            this.gbxheadSuplier.Location = new System.Drawing.Point(0, 53);
            this.gbxheadSuplier.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.gbxheadSuplier.Name = "gbxheadSuplier";
            this.gbxheadSuplier.Padding = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.gbxheadSuplier.Size = new System.Drawing.Size(1041, 299);
            this.gbxheadSuplier.TabIndex = 0;
            this.gbxheadSuplier.TabStop = false;
            this.gbxheadSuplier.Text = "Cập nhật đơn vị cung cấp";
            this.gbxheadSuplier.Enter += new System.EventHandler(this.gbxheadSuplier_Enter);
            // 
            // dtpCreate_atSuplier
            // 
            this.dtpCreate_atSuplier.Location = new System.Drawing.Point(38, 255);
            this.dtpCreate_atSuplier.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.dtpCreate_atSuplier.Name = "dtpCreate_atSuplier";
            this.dtpCreate_atSuplier.Size = new System.Drawing.Size(294, 32);
            this.dtpCreate_atSuplier.TabIndex = 22;
            // 
            // tbxAddressSuplier
            // 
            this.tbxAddressSuplier.Location = new System.Drawing.Point(722, 255);
            this.tbxAddressSuplier.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.tbxAddressSuplier.Name = "tbxAddressSuplier";
            this.tbxAddressSuplier.Size = new System.Drawing.Size(268, 32);
            this.tbxAddressSuplier.TabIndex = 15;
            // 
            // lbAddressSuplier
            // 
            this.lbAddressSuplier.AutoSize = true;
            this.lbAddressSuplier.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(224)))), ((int)(((byte)(192)))));
            this.lbAddressSuplier.Font = new System.Drawing.Font("Segoe UI", 11F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point);
            this.lbAddressSuplier.Location = new System.Drawing.Point(722, 224);
            this.lbAddressSuplier.Name = "lbAddressSuplier";
            this.lbAddressSuplier.Padding = new System.Windows.Forms.Padding(0, 0, 67, 0);
            this.lbAddressSuplier.Size = new System.Drawing.Size(140, 25);
            this.lbAddressSuplier.TabIndex = 13;
            this.lbAddressSuplier.Text = "Địa Chỉ";
            // 
            // lbCreate_atSuplier
            // 
            this.lbCreate_atSuplier.AutoSize = true;
            this.lbCreate_atSuplier.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(224)))), ((int)(((byte)(192)))));
            this.lbCreate_atSuplier.Font = new System.Drawing.Font("Segoe UI", 11F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point);
            this.lbCreate_atSuplier.Location = new System.Drawing.Point(38, 224);
            this.lbCreate_atSuplier.Name = "lbCreate_atSuplier";
            this.lbCreate_atSuplier.Padding = new System.Windows.Forms.Padding(0, 0, 53, 0);
            this.lbCreate_atSuplier.Size = new System.Drawing.Size(145, 25);
            this.lbCreate_atSuplier.TabIndex = 21;
            this.lbCreate_atSuplier.Text = "Ngày Tạo";
            // 
            // tbxNameSuplier
            // 
            this.tbxNameSuplier.Location = new System.Drawing.Point(38, 172);
            this.tbxNameSuplier.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.tbxNameSuplier.Name = "tbxNameSuplier";
            this.tbxNameSuplier.Size = new System.Drawing.Size(294, 32);
            this.tbxNameSuplier.TabIndex = 20;
            // 
            // lbNameSuplier
            // 
            this.lbNameSuplier.AutoSize = true;
            this.lbNameSuplier.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(224)))), ((int)(((byte)(192)))));
            this.lbNameSuplier.Font = new System.Drawing.Font("Segoe UI", 11F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point);
            this.lbNameSuplier.Location = new System.Drawing.Point(38, 141);
            this.lbNameSuplier.Name = "lbNameSuplier";
            this.lbNameSuplier.Size = new System.Drawing.Size(169, 25);
            this.lbNameSuplier.TabIndex = 19;
            this.lbNameSuplier.Text = "Tên Nhà Cung Cấp";
            // 
            // tbxEmailSuplier
            // 
            this.tbxEmailSuplier.Location = new System.Drawing.Point(722, 81);
            this.tbxEmailSuplier.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.tbxEmailSuplier.Name = "tbxEmailSuplier";
            this.tbxEmailSuplier.Size = new System.Drawing.Size(268, 32);
            this.tbxEmailSuplier.TabIndex = 18;
            // 
            // lbEmailSuplier
            // 
            this.lbEmailSuplier.AutoSize = true;
            this.lbEmailSuplier.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(224)))), ((int)(((byte)(192)))));
            this.lbEmailSuplier.Font = new System.Drawing.Font("Segoe UI", 11F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point);
            this.lbEmailSuplier.Location = new System.Drawing.Point(722, 51);
            this.lbEmailSuplier.Name = "lbEmailSuplier";
            this.lbEmailSuplier.Padding = new System.Windows.Forms.Padding(0, 0, 80, 0);
            this.lbEmailSuplier.Size = new System.Drawing.Size(138, 25);
            this.lbEmailSuplier.TabIndex = 17;
            this.lbEmailSuplier.Text = "Email";
            // 
            // tbxPhoneSuplier
            // 
            this.tbxPhoneSuplier.Location = new System.Drawing.Point(722, 172);
            this.tbxPhoneSuplier.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.tbxPhoneSuplier.Name = "tbxPhoneSuplier";
            this.tbxPhoneSuplier.Size = new System.Drawing.Size(268, 32);
            this.tbxPhoneSuplier.TabIndex = 16;
            // 
            // lbPhoneSuplier
            // 
            this.lbPhoneSuplier.AutoSize = true;
            this.lbPhoneSuplier.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(224)))), ((int)(((byte)(192)))));
            this.lbPhoneSuplier.Font = new System.Drawing.Font("Segoe UI", 11F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point);
            this.lbPhoneSuplier.Location = new System.Drawing.Point(722, 141);
            this.lbPhoneSuplier.Name = "lbPhoneSuplier";
            this.lbPhoneSuplier.Padding = new System.Windows.Forms.Padding(0, 0, 16, 0);
            this.lbPhoneSuplier.Size = new System.Drawing.Size(145, 25);
            this.lbPhoneSuplier.TabIndex = 14;
            this.lbPhoneSuplier.Text = "Số Điện Thoại";
            // 
            // tbxIdSuplier
            // 
            this.tbxIdSuplier.Location = new System.Drawing.Point(38, 81);
            this.tbxIdSuplier.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.tbxIdSuplier.Name = "tbxIdSuplier";
            this.tbxIdSuplier.Size = new System.Drawing.Size(294, 32);
            this.tbxIdSuplier.TabIndex = 12;
            // 
            // lbIdSuplier
            // 
            this.lbIdSuplier.AutoSize = true;
            this.lbIdSuplier.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(224)))), ((int)(((byte)(192)))));
            this.lbIdSuplier.Font = new System.Drawing.Font("Segoe UI", 11F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point);
            this.lbIdSuplier.Location = new System.Drawing.Point(38, 51);
            this.lbIdSuplier.Name = "lbIdSuplier";
            this.lbIdSuplier.Size = new System.Drawing.Size(167, 25);
            this.lbIdSuplier.TabIndex = 11;
            this.lbIdSuplier.Text = "Mã Nhà Cung Cấp";
            // 
            // panel1
            // 
            this.panel1.Controls.Add(this.lbSuplier);
            this.panel1.Controls.Add(this.gbxheadSuplier);
            this.panel1.Location = new System.Drawing.Point(22, 15);
            this.panel1.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(1041, 365);
            this.panel1.TabIndex = 1;
            // 
            // lbSuplier
            // 
            this.lbSuplier.AutoSize = true;
            this.lbSuplier.Font = new System.Drawing.Font("Segoe UI", 20F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point);
            this.lbSuplier.Location = new System.Drawing.Point(330, 0);
            this.lbSuplier.Name = "lbSuplier";
            this.lbSuplier.Size = new System.Drawing.Size(422, 46);
            this.lbSuplier.TabIndex = 1;
            this.lbSuplier.Text = "Danh Sách Nhà Cung Cấp";
            this.lbSuplier.Click += new System.EventHandler(this.label1_Click);
            // 
            // dgvSuplier
            // 
            this.dgvSuplier.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgvSuplier.Location = new System.Drawing.Point(18, 375);
            this.dgvSuplier.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.dgvSuplier.Name = "dgvSuplier";
            this.dgvSuplier.RowHeadersWidth = 51;
            this.dgvSuplier.RowTemplate.Height = 25;
            this.dgvSuplier.Size = new System.Drawing.Size(1041, 249);
            this.dgvSuplier.TabIndex = 2;
            this.dgvSuplier.CellClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.dgvSuplier_CellClick);
            // 
            // btnExitSuplier
            // 
            this.btnExitSuplier.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(192)))), ((int)(((byte)(128)))));
            this.btnExitSuplier.Location = new System.Drawing.Point(760, 637);
            this.btnExitSuplier.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.btnExitSuplier.Name = "btnExitSuplier";
            this.btnExitSuplier.Size = new System.Drawing.Size(115, 47);
            this.btnExitSuplier.TabIndex = 19;
            this.btnExitSuplier.Text = "Thoát";
            this.btnExitSuplier.UseVisualStyleBackColor = false;
            // 
            // btnEditSuplier
            // 
            this.btnEditSuplier.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(192)))), ((int)(((byte)(128)))));
            this.btnEditSuplier.Location = new System.Drawing.Point(470, 637);
            this.btnEditSuplier.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.btnEditSuplier.Name = "btnEditSuplier";
            this.btnEditSuplier.Size = new System.Drawing.Size(126, 47);
            this.btnEditSuplier.TabIndex = 18;
            this.btnEditSuplier.Text = "Sửa";
            this.btnEditSuplier.UseVisualStyleBackColor = false;
            this.btnEditSuplier.Click += new System.EventHandler(this.btnEditSuplier_Click);
            // 
            // btnDeleteSuplier
            // 
            this.btnDeleteSuplier.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(192)))), ((int)(((byte)(128)))));
            this.btnDeleteSuplier.Location = new System.Drawing.Point(335, 637);
            this.btnDeleteSuplier.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.btnDeleteSuplier.Name = "btnDeleteSuplier";
            this.btnDeleteSuplier.Size = new System.Drawing.Size(112, 47);
            this.btnDeleteSuplier.TabIndex = 16;
            this.btnDeleteSuplier.Text = "Xóa";
            this.btnDeleteSuplier.UseVisualStyleBackColor = false;
            this.btnDeleteSuplier.Click += new System.EventHandler(this.btnDeleteSuplier_Click);
            // 
            // btnAddSuplier
            // 
            this.btnAddSuplier.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(192)))), ((int)(((byte)(128)))));
            this.btnAddSuplier.Location = new System.Drawing.Point(197, 637);
            this.btnAddSuplier.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.btnAddSuplier.Name = "btnAddSuplier";
            this.btnAddSuplier.Size = new System.Drawing.Size(117, 47);
            this.btnAddSuplier.TabIndex = 15;
            this.btnAddSuplier.Text = "Tạo Mới";
            this.btnAddSuplier.UseVisualStyleBackColor = false;
            this.btnAddSuplier.Click += new System.EventHandler(this.btnAddSuplier_Click);
            // 
            // btnSaveSuplier
            // 
            this.btnSaveSuplier.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(192)))), ((int)(((byte)(128)))));
            this.btnSaveSuplier.Location = new System.Drawing.Point(616, 637);
            this.btnSaveSuplier.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.btnSaveSuplier.Name = "btnSaveSuplier";
            this.btnSaveSuplier.Size = new System.Drawing.Size(114, 47);
            this.btnSaveSuplier.TabIndex = 17;
            this.btnSaveSuplier.Text = "Lưu";
            this.btnSaveSuplier.UseVisualStyleBackColor = false;
            // 
            // frmSuplier
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 20F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1073, 695);
            this.Controls.Add(this.btnExitSuplier);
            this.Controls.Add(this.btnEditSuplier);
            this.Controls.Add(this.btnDeleteSuplier);
            this.Controls.Add(this.btnAddSuplier);
            this.Controls.Add(this.btnSaveSuplier);
            this.Controls.Add(this.dgvSuplier);
            this.Controls.Add(this.panel1);
            this.Name = "frmSuplier";
            this.Text = "Nhà cung cấp";
            this.gbxheadSuplier.ResumeLayout(false);
            this.gbxheadSuplier.PerformLayout();
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgvSuplier)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.GroupBox gbxheadSuplier;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Label lbSuplier;
        private System.Windows.Forms.DataGridView dgvSuplier;
        private System.Windows.Forms.DateTimePicker dtpCreate_atSuplier;
        private System.Windows.Forms.TextBox tbxAddressSuplier;
        private System.Windows.Forms.Label lbAddressSuplier;
        private System.Windows.Forms.Label lbCreate_atSuplier;
        private System.Windows.Forms.TextBox tbxNameSuplier;
        private System.Windows.Forms.Label lbNameSuplier;
        private System.Windows.Forms.TextBox tbxEmailSuplier;
        private System.Windows.Forms.Label lbEmailSuplier;
        private System.Windows.Forms.TextBox tbxPhoneSuplier;
        private System.Windows.Forms.Label lbPhoneSuplier;
        private System.Windows.Forms.TextBox tbxIdSuplier;
        private System.Windows.Forms.Label lbIdSuplier;
        private System.Windows.Forms.Button btnExitSuplier;
        private System.Windows.Forms.Button btnEditSuplier;
        private System.Windows.Forms.Button btnDeleteSuplier;
        private System.Windows.Forms.Button btnAddSuplier;
        private System.Windows.Forms.Button btnSaveSuplier;
    }
}