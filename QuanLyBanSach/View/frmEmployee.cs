﻿using QuanLyBanSach.Models;
using QuanLyBanSach.Services;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace QuanLyBanSach.View
{
    public partial class frmEmployee : Form
    {
        public frmEmployee()
        {
            InitializeComponent();
            LoadData();
        }
        private void LoadData()
        {
            dgvEmployee.DataSource = EmployeeServices.Instance.GetAll().Select(x => new
                                    {
                                        Id = x.Id,
                                        UserName = x.UserName,
                                        Name = x.Name,
                                        PhoneNumber = x.PhoneNumber,
                                        RoleName = x.RoleNaviagtion.Name,
                                        Address = x.Address
                                    }).ToList();
            dgvEmployee.Columns[0].HeaderText = "Mã Nhân Viên";
            dgvEmployee.Columns[1].HeaderText = "Tên Đăng Nhập";
            dgvEmployee.Columns[2].HeaderText = "Họ Và Tên";
            dgvEmployee.Columns[3].HeaderText = "Số Điện Thoại";
            dgvEmployee.Columns[4].HeaderText = "Chức Vụ";
            dgvEmployee.Columns[5].HeaderText = "Địa Chỉ";
            cbRole.DataSource = RoleServices.Instance.GetAll();
            cbRole.DisplayMember = "Name";
            cbRole.ValueMember = "Id";
        }
         

       

        private void btnAddEmployee_Click(object sender, EventArgs e)
        {
            Employee emp = new Employee()
            {
                UserName = txbUserName.Text,
                Password = "1",
                Address = txbAddress.Text,
                Name = txbName.Text,
                RoleId = Convert.ToInt32(cbRole.SelectedValue),
                PhoneNumber = txbPhoneNumber.Text
            };
            EmployeeServices.Instance.Insert(emp);
        }
    }
}
