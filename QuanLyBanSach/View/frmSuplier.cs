﻿using QuanLyBanSach.Models;
using QuanLyBanSach.Services;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace QuanLyBanSach.View
{
    public partial class frmSuplier : Form
    {
        public frmSuplier()
        {
            InitializeComponent();
            LoadData();
        }
        private void LoadData()
        {
            dgvSuplier.DataSource = SupplierServices.Instance.GetAll().Select(x => new
            {
                x.Id,
                x.Name,
                x.Address,
                x.PhoneNumber,
                x.Email
            }).ToList();
            dgvSuplier.Columns[0].HeaderText = "Mã";
            dgvSuplier.Columns[1].HeaderText = "Tên Nhà Cung Cấp";
            dgvSuplier.Columns[2].HeaderText = "Địa Chỉ";
            dgvSuplier.Columns[3].HeaderText = "Số Điện Thoại";
            dgvSuplier.Columns[4].HeaderText = "Email";
        }

        private void label1_Click(object sender, EventArgs e)
        {

        }

        private void gbxheadSuplier_Enter(object sender, EventArgs e)
        {

        }


        private void btnAddSuplier_Click(object sender, EventArgs e)
        {
            Supplier supplier = new Supplier()
            {
                Name = tbxNameSuplier.Text,
                PhoneNumber = tbxPhoneSuplier.Text,
                Email = tbxEmailSuplier.Text,
                Address = tbxAddressSuplier.Text
            };
            SupplierServices.Instance.Insert(supplier);
        }

        private void btnDeleteSuplier_Click(object sender, EventArgs e)
        {
            Supplier supplier = new Supplier()
            {
                Id = CellClickId
            };
            SupplierServices.Instance.Delete(supplier);
        }
        int CellClickId;
        private void dgvSuplier_CellClick(object sender, DataGridViewCellEventArgs e)
        {
            int a = e.RowIndex;
            CellClickId = Convert.ToInt32(dgvSuplier.Rows[a].Cells[0].Value);
            tbxNameSuplier.Text = dgvSuplier.Rows[a].Cells[1].Value.ToString();
            tbxAddressSuplier.Text = dgvSuplier.Rows[a].Cells[2].Value.ToString();
            tbxPhoneSuplier.Text = dgvSuplier.Rows[a].Cells[3].Value.ToString();
            tbxEmailSuplier.Text = dgvSuplier.Rows[a].Cells[4].Value.ToString(); 
        }

        private void btnEditSuplier_Click(object sender, EventArgs e)
        {
            Supplier supplier = new Supplier()
            {
                Id = CellClickId,
                Name = tbxNameSuplier.Text,
                Address = tbxAddressSuplier.Text,
                Email = tbxEmailSuplier.Text,
                PhoneNumber = tbxPhoneSuplier.Text
            };
            SupplierServices.Instance.Update(supplier);
        }
    }
}
