﻿
namespace QuanLyBanSach.View
{
    partial class frmEmployee
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.btnExitEmployee = new System.Windows.Forms.Button();
            this.btnDeleteEmployee = new System.Windows.Forms.Button();
            this.dgvEmployee = new System.Windows.Forms.DataGridView();
            this.btnAddEmployee = new System.Windows.Forms.Button();
            this.btnSaveEmployee = new System.Windows.Forms.Button();
            this.btnEditEmployee = new System.Windows.Forms.Button();
            this.txbAddress = new System.Windows.Forms.TextBox();
            this.lbAddressEmployee = new System.Windows.Forms.Label();
            this.lbCreate_atEmployee = new System.Windows.Forms.Label();
            this.txbName = new System.Windows.Forms.TextBox();
            this.txbEmail = new System.Windows.Forms.TextBox();
            this.txbPhoneNumber = new System.Windows.Forms.TextBox();
            this.lbPhoneEmployee = new System.Windows.Forms.Label();
            this.txbUserName = new System.Windows.Forms.TextBox();
            this.lblUserName = new System.Windows.Forms.Label();
            this.lbListEmployee = new System.Windows.Forms.Label();
            this.panel1 = new System.Windows.Forms.Panel();
            this.lbNameEmployee = new System.Windows.Forms.Label();
            this.lbEmailEmployee = new System.Windows.Forms.Label();
            this.cbRole = new System.Windows.Forms.ComboBox();
            ((System.ComponentModel.ISupportInitialize)(this.dgvEmployee)).BeginInit();
            this.panel1.SuspendLayout();
            this.SuspendLayout();
            // 
            // btnExitEmployee
            // 
            this.btnExitEmployee.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(192)))), ((int)(((byte)(128)))));
            this.btnExitEmployee.Location = new System.Drawing.Point(706, 615);
            this.btnExitEmployee.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.btnExitEmployee.Name = "btnExitEmployee";
            this.btnExitEmployee.Size = new System.Drawing.Size(115, 47);
            this.btnExitEmployee.TabIndex = 20;
            this.btnExitEmployee.Text = "Thoát";
            this.btnExitEmployee.UseVisualStyleBackColor = false;
            // 
            // btnDeleteEmployee
            // 
            this.btnDeleteEmployee.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(192)))), ((int)(((byte)(128)))));
            this.btnDeleteEmployee.Location = new System.Drawing.Point(281, 615);
            this.btnDeleteEmployee.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.btnDeleteEmployee.Name = "btnDeleteEmployee";
            this.btnDeleteEmployee.Size = new System.Drawing.Size(112, 47);
            this.btnDeleteEmployee.TabIndex = 17;
            this.btnDeleteEmployee.Text = "Xóa";
            this.btnDeleteEmployee.UseVisualStyleBackColor = false;
            // 
            // dgvEmployee
            // 
            this.dgvEmployee.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgvEmployee.Location = new System.Drawing.Point(0, 320);
            this.dgvEmployee.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.dgvEmployee.Name = "dgvEmployee";
            this.dgvEmployee.RowHeadersWidth = 51;
            this.dgvEmployee.RowTemplate.Height = 25;
            this.dgvEmployee.Size = new System.Drawing.Size(1055, 261);
            this.dgvEmployee.TabIndex = 11;
            // 
            // btnAddEmployee
            // 
            this.btnAddEmployee.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(192)))), ((int)(((byte)(128)))));
            this.btnAddEmployee.Location = new System.Drawing.Point(143, 615);
            this.btnAddEmployee.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.btnAddEmployee.Name = "btnAddEmployee";
            this.btnAddEmployee.Size = new System.Drawing.Size(117, 47);
            this.btnAddEmployee.TabIndex = 16;
            this.btnAddEmployee.Text = "Tạo Mới";
            this.btnAddEmployee.UseVisualStyleBackColor = false;
            this.btnAddEmployee.Click += new System.EventHandler(this.btnAddEmployee_Click);
            // 
            // btnSaveEmployee
            // 
            this.btnSaveEmployee.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(192)))), ((int)(((byte)(128)))));
            this.btnSaveEmployee.Location = new System.Drawing.Point(562, 615);
            this.btnSaveEmployee.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.btnSaveEmployee.Name = "btnSaveEmployee";
            this.btnSaveEmployee.Size = new System.Drawing.Size(114, 47);
            this.btnSaveEmployee.TabIndex = 18;
            this.btnSaveEmployee.Text = "Lưu";
            this.btnSaveEmployee.UseVisualStyleBackColor = false;
            // 
            // btnEditEmployee
            // 
            this.btnEditEmployee.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(192)))), ((int)(((byte)(128)))));
            this.btnEditEmployee.Location = new System.Drawing.Point(416, 615);
            this.btnEditEmployee.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.btnEditEmployee.Name = "btnEditEmployee";
            this.btnEditEmployee.Size = new System.Drawing.Size(126, 47);
            this.btnEditEmployee.TabIndex = 19;
            this.btnEditEmployee.Text = "Sửa";
            this.btnEditEmployee.UseVisualStyleBackColor = false;
            // 
            // txbAddress
            // 
            this.txbAddress.Location = new System.Drawing.Point(709, 264);
            this.txbAddress.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.txbAddress.Name = "txbAddress";
            this.txbAddress.Size = new System.Drawing.Size(268, 27);
            this.txbAddress.TabIndex = 4;
            // 
            // lbAddressEmployee
            // 
            this.lbAddressEmployee.AutoSize = true;
            this.lbAddressEmployee.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(224)))), ((int)(((byte)(192)))));
            this.lbAddressEmployee.Font = new System.Drawing.Font("Segoe UI", 11F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point);
            this.lbAddressEmployee.Location = new System.Drawing.Point(709, 232);
            this.lbAddressEmployee.Name = "lbAddressEmployee";
            this.lbAddressEmployee.Padding = new System.Windows.Forms.Padding(0, 0, 51, 0);
            this.lbAddressEmployee.Size = new System.Drawing.Size(124, 25);
            this.lbAddressEmployee.TabIndex = 3;
            this.lbAddressEmployee.Text = "Địa Chỉ";
            // 
            // lbCreate_atEmployee
            // 
            this.lbCreate_atEmployee.AutoSize = true;
            this.lbCreate_atEmployee.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(224)))), ((int)(((byte)(192)))));
            this.lbCreate_atEmployee.Font = new System.Drawing.Font("Segoe UI", 11F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point);
            this.lbCreate_atEmployee.Location = new System.Drawing.Point(37, 232);
            this.lbCreate_atEmployee.Name = "lbCreate_atEmployee";
            this.lbCreate_atEmployee.Padding = new System.Windows.Forms.Padding(0, 0, 35, 0);
            this.lbCreate_atEmployee.Size = new System.Drawing.Size(102, 25);
            this.lbCreate_atEmployee.TabIndex = 9;
            this.lbCreate_atEmployee.Text = "Quyền";
            // 
            // txbName
            // 
            this.txbName.Location = new System.Drawing.Point(37, 177);
            this.txbName.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.txbName.Name = "txbName";
            this.txbName.Size = new System.Drawing.Size(294, 27);
            this.txbName.TabIndex = 8;
            // 
            // txbEmail
            // 
            this.txbEmail.Location = new System.Drawing.Point(709, 91);
            this.txbEmail.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.txbEmail.Name = "txbEmail";
            this.txbEmail.Size = new System.Drawing.Size(268, 27);
            this.txbEmail.TabIndex = 6;
            // 
            // txbPhoneNumber
            // 
            this.txbPhoneNumber.Location = new System.Drawing.Point(709, 177);
            this.txbPhoneNumber.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.txbPhoneNumber.Name = "txbPhoneNumber";
            this.txbPhoneNumber.Size = new System.Drawing.Size(268, 27);
            this.txbPhoneNumber.TabIndex = 4;
            // 
            // lbPhoneEmployee
            // 
            this.lbPhoneEmployee.AutoSize = true;
            this.lbPhoneEmployee.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(224)))), ((int)(((byte)(192)))));
            this.lbPhoneEmployee.Font = new System.Drawing.Font("Segoe UI", 11F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point);
            this.lbPhoneEmployee.Location = new System.Drawing.Point(709, 147);
            this.lbPhoneEmployee.Name = "lbPhoneEmployee";
            this.lbPhoneEmployee.Size = new System.Drawing.Size(129, 25);
            this.lbPhoneEmployee.TabIndex = 3;
            this.lbPhoneEmployee.Text = "Số Điện Thoại";
            // 
            // txbUserName
            // 
            this.txbUserName.Location = new System.Drawing.Point(37, 99);
            this.txbUserName.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.txbUserName.Name = "txbUserName";
            this.txbUserName.Size = new System.Drawing.Size(294, 27);
            this.txbUserName.TabIndex = 2;
            // 
            // lblUserName
            // 
            this.lblUserName.AutoSize = true;
            this.lblUserName.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(224)))), ((int)(((byte)(192)))));
            this.lblUserName.Font = new System.Drawing.Font("Segoe UI", 11F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point);
            this.lblUserName.Location = new System.Drawing.Point(37, 67);
            this.lblUserName.Name = "lblUserName";
            this.lblUserName.Size = new System.Drawing.Size(105, 25);
            this.lblUserName.TabIndex = 1;
            this.lblUserName.Text = "User Name";
            // 
            // lbListEmployee
            // 
            this.lbListEmployee.AutoSize = true;
            this.lbListEmployee.Font = new System.Drawing.Font("Segoe UI", 20F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point);
            this.lbListEmployee.Location = new System.Drawing.Point(368, 0);
            this.lbListEmployee.Name = "lbListEmployee";
            this.lbListEmployee.Size = new System.Drawing.Size(362, 46);
            this.lbListEmployee.TabIndex = 0;
            this.lbListEmployee.Text = "Danh Sách Nhân Viên";
            // 
            // panel1
            // 
            this.panel1.Controls.Add(this.cbRole);
            this.panel1.Controls.Add(this.dgvEmployee);
            this.panel1.Controls.Add(this.txbAddress);
            this.panel1.Controls.Add(this.lbAddressEmployee);
            this.panel1.Controls.Add(this.lbCreate_atEmployee);
            this.panel1.Controls.Add(this.txbName);
            this.panel1.Controls.Add(this.lbNameEmployee);
            this.panel1.Controls.Add(this.txbEmail);
            this.panel1.Controls.Add(this.lbEmailEmployee);
            this.panel1.Controls.Add(this.txbPhoneNumber);
            this.panel1.Controls.Add(this.lbPhoneEmployee);
            this.panel1.Controls.Add(this.txbUserName);
            this.panel1.Controls.Add(this.lblUserName);
            this.panel1.Controls.Add(this.lbListEmployee);
            this.panel1.Location = new System.Drawing.Point(17, 12);
            this.panel1.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(1055, 595);
            this.panel1.TabIndex = 15;
            // 
            // lbNameEmployee
            // 
            this.lbNameEmployee.AutoSize = true;
            this.lbNameEmployee.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(224)))), ((int)(((byte)(192)))));
            this.lbNameEmployee.Font = new System.Drawing.Font("Segoe UI", 11F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point);
            this.lbNameEmployee.Location = new System.Drawing.Point(37, 147);
            this.lbNameEmployee.Name = "lbNameEmployee";
            this.lbNameEmployee.Size = new System.Drawing.Size(135, 25);
            this.lbNameEmployee.TabIndex = 7;
            this.lbNameEmployee.Text = "Tên Nhân Viên";
            // 
            // lbEmailEmployee
            // 
            this.lbEmailEmployee.AutoSize = true;
            this.lbEmailEmployee.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(224)))), ((int)(((byte)(192)))));
            this.lbEmailEmployee.Font = new System.Drawing.Font("Segoe UI", 11F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point);
            this.lbEmailEmployee.Location = new System.Drawing.Point(709, 60);
            this.lbEmailEmployee.Name = "lbEmailEmployee";
            this.lbEmailEmployee.Padding = new System.Windows.Forms.Padding(0, 0, 64, 0);
            this.lbEmailEmployee.Size = new System.Drawing.Size(122, 25);
            this.lbEmailEmployee.TabIndex = 5;
            this.lbEmailEmployee.Text = "Email";
            // 
            // cbRole
            // 
            this.cbRole.FormattingEnabled = true;
            this.cbRole.Location = new System.Drawing.Point(37, 264);
            this.cbRole.Name = "cbRole";
            this.cbRole.Size = new System.Drawing.Size(294, 28);
            this.cbRole.TabIndex = 12;
            // 
            // frmEmployee
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 20F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1089, 672);
            this.Controls.Add(this.btnExitEmployee);
            this.Controls.Add(this.btnDeleteEmployee);
            this.Controls.Add(this.btnAddEmployee);
            this.Controls.Add(this.btnSaveEmployee);
            this.Controls.Add(this.btnEditEmployee);
            this.Controls.Add(this.panel1);
            this.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.Name = "frmEmployee";
            this.Text = "frmEmployee";
            ((System.ComponentModel.ISupportInitialize)(this.dgvEmployee)).EndInit();
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Button btnExitEmployee;
        private System.Windows.Forms.Button btnDeleteEmployee;
        private System.Windows.Forms.DataGridView dgvEmployee;
        private System.Windows.Forms.Button btnAddEmployee;
        private System.Windows.Forms.Button btnSaveEmployee;
        private System.Windows.Forms.Button btnEditEmployee;
        private System.Windows.Forms.TextBox txbAddress;
        private System.Windows.Forms.Label lbAddressEmployee;
        private System.Windows.Forms.Label lbCreate_atEmployee;
        private System.Windows.Forms.TextBox txbName;
        private System.Windows.Forms.TextBox txbEmail;
        private System.Windows.Forms.TextBox txbPhoneNumber;
        private System.Windows.Forms.Label lbPhoneEmployee;
        private System.Windows.Forms.TextBox txbUserName;
        private System.Windows.Forms.Label lblUserName;
        private System.Windows.Forms.Label lbListEmployee;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Label lbNameEmployee;
        private System.Windows.Forms.Label lbEmailEmployee;
        private System.Windows.Forms.ComboBox cbRole;
    }
}