﻿using Microsoft.EntityFrameworkCore;
using QuanLyBanSach.Data;
using QuanLyBanSach.Services;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data; 
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace QuanLyBanSach.View
{
    public partial class frmHome : Form
    {
        public frmHome()
        {
            InitializeComponent();
            LoadData();
            btnUsers.Text =  EmployeeServices.Instance.GetEmployeeCurrent().Name;
            if (EmployeeServices.Instance.IsAdmin(EmployeeServices.Instance.GetEmployeeCurrent().UserName))
            {
                btnUsers.Enabled = true;
            }
            else
            {
                btnUsers.Enabled = false;
            }
        }
        public void LoadData()
        {
            dgvHome.DataSource = BookServices.Instance.GetAll().Select(x =>
                            new
                            {
                                ID = x.Id,
                                Name = x.Name,
                                PublisherName = x.PublisherNavigation.Name,
                                AuthourName = x.AuthorNavigation.Name,
                                CategoryName = x.CategoryNavigation.Name,
                                Price = x.Price,
                                Count = x.Count
                            }).ToList();
            dgvHome.Columns[0].HeaderText = "Mã Sách";
            dgvHome.Columns[1].HeaderText = "Tên Sách";
            dgvHome.Columns[2].HeaderText = "Nhà Xuất Bản";
            dgvHome.Columns[3].HeaderText = "Tác Giả";
            dgvHome.Columns[4].HeaderText = "Thể Loại";
            dgvHome.Columns[6].HeaderText = "Số Lượng";
            dgvHome.Columns[5].HeaderText = "Giá";
        }

        private void btnImport_Click(object sender, EventArgs e)
        {
            View.frmImportDetail ipd = new View.frmImportDetail();
            this.Hide();
            ipd.ShowDialog();
            this.Show();
        }

        private void btnUsers_Click(object sender, EventArgs e)
        {
            View.frmEmployee frmemployee = new View.frmEmployee();
            this.Hide();
            frmemployee.ShowDialog();
            this.Show();
        }

        private void btnCustomer_Click(object sender, EventArgs e)
        {
            View.frmCustomer cus = new View.frmCustomer();
            this.Hide();
            cus.ShowDialog();
            this.Show();
        }

        private void btnExports_Click(object sender, EventArgs e)
        {
            View.frmBillDetail bd = new View.frmBillDetail();
            this.Hide();
            bd.ShowDialog();
            this.Show();
        }

        private void btnSupplier_Click(object sender, EventArgs e)
        {
            View.frmSuplier frmSuplier = new View.frmSuplier();
            this.Hide();
            frmSuplier.ShowDialog();
            this.Show();
        }

        private void btnBook_Click(object sender, EventArgs e)
        {
            frmBook frmBook = new frmBook();
            this.Hide();
            frmBook.ShowDialog();
            this.Show();
        }

        private void btnFilter_Click(object sender, EventArgs e)
        {

        }
    }
}
