﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Linq;
using LinqKit;
using QuanLyBanSach.Data;
using QuanLyBanSach.Models;
using Microsoft.EntityFrameworkCore;

namespace QuanLyBanSach.Services
{
    class EmployeeServices
    {
        private static EmployeeServices instance;
        private string userName;
        public static EmployeeServices Instance
        {
            get { if (instance == null) instance = new EmployeeServices(); return instance; }
            private set { EmployeeServices.instance = value; }
        }
        private EmployeeServices() {  }

        public List<Employee> GetAll()
        {
            using (var context = new QuanLyBanSachContext())
            {
                return context.Employees
                    .Include(x=>x.RoleNaviagtion) 
                    .OrderByDescending(x => x.CreateAt).ToList();
            }
        }
        public void Insert(Employee employee)
        {
            using (var context = new QuanLyBanSachContext())
            {
                context.Employees.Add(employee);
                context.SaveChanges();
            }
        }
        public void Update(Employee employee)
        {
            using (var context = new QuanLyBanSachContext())
            {
                context.Employees.Update(employee);
                context.SaveChanges();
            }
        }
        public void Delete(Employee employee)
        {
            using (var context = new QuanLyBanSachContext())
            {
                context.Employees.Remove(employee);
                context.SaveChanges();
            }
        }
        public List<Employee> Filter(string name, string phonenumber)
        {
            using (var context = new QuanLyBanSachContext())
            {
                var filterCondition = PredicateBuilder.New<Employee>();

                if (!String.IsNullOrWhiteSpace(name)) filterCondition.And(c => c.Name == name);
                if (!String.IsNullOrWhiteSpace(phonenumber)) filterCondition.And(c => c.PhoneNumber == phonenumber);

                if (String.IsNullOrWhiteSpace(name) &&
                    String.IsNullOrWhiteSpace(phonenumber))
                {
                    filterCondition.And(c => true);
                }
                return context.Employees
                                         .Where(filterCondition)
                                         .OrderByDescending(c => c.CreateAt)
                                         .ToList();
            }
        }
        public bool CheckLogin(string UserName, string Password)
        {
            using(var context = new QuanLyBanSachContext())
            {
                if(context.Employees.Where(x => x.UserName == UserName && x.Password == Password).Count() == 0)
                { 
                    return false;
                }
            }
            userName = UserName;
            return true;
        }
        public Employee GetEmployeeCurrent()
        {
            using(var context = new QuanLyBanSachContext())
            { 
                return context.Employees.Where(x => x.UserName == userName).FirstOrDefault();
            }
        }
        public bool IsAdmin(string userName)
        {
            using(var context = new QuanLyBanSachContext())
            {
                if(context.Employees.Where(x=>x.RoleId == 1 && x.UserName == userName).Count() == 1)
                {
                    return true;
                }
                return false;
            }
        }
    }
}
