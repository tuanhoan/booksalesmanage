﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Linq;
using QuanLyBanSach.Models;
using QuanLyBanSach.Data;
using LinqKit;
using Microsoft.EntityFrameworkCore;

namespace QuanLyBanSach.Services
{
    class BillServices
    {
        private static BillServices instance; 
        public static BillServices Instance
        {
            get { if (instance == null) instance = new BillServices(); return instance; }
            private set { BillServices.instance = value; }
        }
        private BillServices() { }

        public List<Bill> GetAll()
        {
            using (var context = new QuanLyBanSachContext())
            {
                return context.Bills
                    .Include(x => x.CustomerNavigation)
                    .OrderByDescending(x => x.CreateAt).ToList();
            }
        }
        public void Insert(Bill bill)
        {
            using (var context = new QuanLyBanSachContext())
            {
                context.Bills.Add(bill);
                context.SaveChanges();
            }
        }
        public void Update(Bill bill)
        {
            using (var context = new QuanLyBanSachContext())
            {
                context.Bills.Update(bill);
                context.SaveChanges();
            }
        }
        public void Delete(Bill bill)
        {
            using (var context = new QuanLyBanSachContext())
            { 
                context.Bills.Remove(bill);
                context.SaveChanges();
            }
        }
        public List<Bill> Filter(String customerID, String employeeID)
        {
            using (var context = new QuanLyBanSachContext())
            {
                var filterCondition = PredicateBuilder.New<Bill>();

                if (!String.IsNullOrWhiteSpace(customerID)) filterCondition.And(c => c.CustomerId.ToString() == customerID);
                if (!String.IsNullOrWhiteSpace(employeeID)) filterCondition.And(c => c.EmployeeId.ToString() == employeeID);

                if (String.IsNullOrWhiteSpace(customerID) && String.IsNullOrWhiteSpace(employeeID))
                {
                    filterCondition.And(c => true);
                }
                return context.Bills
                                         .Where(filterCondition)
                                         .OrderByDescending(c => c.CreateAt)
                                         .ToList();
            }
        }
    }
}
