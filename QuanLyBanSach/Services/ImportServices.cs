﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using QuanLyBanSach.Data;
using QuanLyBanSach.Models;
using LinqKit;

namespace QuanLyBanSach.Services
{
    class ImportServices
    {
        private static ImportServices instance; 
        public static ImportServices Instance
        {
            get { if (instance == null) instance = new ImportServices(); return instance; }
            private set { ImportServices.instance = value; }
        }
        private ImportServices() {  }

        public List<Import> GetAll()
        {
            using (var context = new QuanLyBanSachContext())
            {
                return context.Imports.OrderByDescending(x => x.CreateAt).ToList();
            }
        }
        public void Insert(Import import)
        {
            using (var context = new QuanLyBanSachContext())
            {
                context.Imports.Add(import);
                context.SaveChanges();
            }
        }
        public void Update(Import import)
        {
            using (var context = new QuanLyBanSachContext())
            {
                context.Imports.Update(import);
                context.SaveChanges();
            }
        }
        public void Delete(Import import)
        {
            using (var context = new QuanLyBanSachContext())
            {
                context.Imports.Remove(import);
                context.SaveChanges();
            }
        }
        public List<Import> Filter(int id_supplier)
        {
            using (var context = new QuanLyBanSachContext())
            {
                return context.Imports.Where(x => x.SupplierId == id_supplier).OrderByDescending(x => x.Id).ToList();
            }
        }
    }
}

