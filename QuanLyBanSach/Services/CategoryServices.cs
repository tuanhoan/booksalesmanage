﻿using System;
using System.Collections.Generic;
using System.Text;
using LinqKit;
using System.Linq;
using QuanLyBanSach.Data;
using QuanLyBanSach.Models;

namespace QuanLyBanSach.Services
{
    class CategoryServices
    {
        private static CategoryServices instance; 
        public static CategoryServices Instance
        {
            get { if (instance == null) instance = new CategoryServices(); return instance; }
            private set { }
        }
        private CategoryServices() {  }

        public List<Category> GetAll()
        {
            using (var context = new QuanLyBanSachContext())
            { 
                return context.Categories.ToList();
            }
        }
        public void Insert(Category category)
        {
            using (var context = new QuanLyBanSachContext())
            {
                context.Categories.Add(category);
                context.SaveChanges();
            }
        }
        public void Update(Category category)
        {
            using (var context = new QuanLyBanSachContext())
            {
                context.Categories.Update(category);
                context.SaveChanges();
            }
        }
        public void Delete(Category category)
        {
            using (var context = new QuanLyBanSachContext())
            {
                context.Categories.Remove(category);
                context.SaveChanges();
            }
        }
        public List<Category> Filter(string name)
        {
            using (var context = new QuanLyBanSachContext())
            {
                var filterCondition = PredicateBuilder.New<Category>();

                if (!String.IsNullOrWhiteSpace(name)) filterCondition.And(c => c.Name == name);

                if (String.IsNullOrWhiteSpace(name))
                {
                    filterCondition.And(c => true);
                }
                return context.Categories
                                         .Where(filterCondition)
                                         .OrderBy(c => c.Name)
                                         .ToList();
            }
        }
    }
}
