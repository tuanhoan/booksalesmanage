﻿using System;
using System.Collections.Generic;
using System.Text;
using QuanLyBanSach.Data;
using LinqKit;
using QuanLyBanSach.Models;
using System.Linq;

namespace QuanLyBanSach.Services
{
    class BillDetailServices
    {
        private static BillDetailServices instance; 
        public static BillDetailServices Instance
        {
            get { if (instance == null) instance = new BillDetailServices(); return instance; }
            private set { BillDetailServices.instance = value; }
        }
        private BillDetailServices() { }

        public List<BillDetail> GetAll()
        {
            using(var context = new QuanLyBanSachContext())
            {
                return context.BillDetails.OrderByDescending(x => x.BookId).ToList();
            } 
        }
        public void Insert(BillDetail billDetail)
        {
            using (var context = new QuanLyBanSachContext())
            {
                context.BillDetails.Add(billDetail);
                context.SaveChanges();
            }
        }
        public void Update(BillDetail billDetail)
        {
            using (var context = new QuanLyBanSachContext())
            {
                context.BillDetails.Update(billDetail);
                context.SaveChanges();
            }
        }
        public void Delete(BillDetail billDetail)
        {
            using (var context = new QuanLyBanSachContext())
            {
                context.BillDetails.Remove(billDetail);
                context.SaveChanges();
            }
        }
    }
}
