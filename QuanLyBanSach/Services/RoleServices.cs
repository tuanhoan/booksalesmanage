﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Linq;
using LinqKit;
using QuanLyBanSach.Data;
using QuanLyBanSach.Models;

namespace QuanLyBanSach.Services
{
    class RoleServices
    {
        private static RoleServices instance; 
        public static RoleServices Instance
        {
            get { if (instance == null) instance = new RoleServices(); return instance; }
            private set { RoleServices.instance = value; }
        }
        private RoleServices() { }

        public List<Role> GetAll()
        {
            using (var context = new QuanLyBanSachContext())
            {
                return context.Roles.ToList();
            }
        }
        public void Insert(Role role)
        {
            using (var context = new QuanLyBanSachContext())
            {
                context.Roles.Add(role);
                context.SaveChanges();
            }
        }
        public void Update(Role role)
        {
            using (var context = new QuanLyBanSachContext())
            {
                context.Roles.Update(role);
                context.SaveChanges();
            }
        }
        public void Delete(Role role)
        {
            using (var context = new QuanLyBanSachContext())
            {
                context.Roles.Remove(role);
                context.SaveChanges();
            }
        }
        public List<Role> Filter(string name)
        {
            using (var context = new QuanLyBanSachContext())
            {
                var filterCondition = PredicateBuilder.New<Role>();

                if (!String.IsNullOrWhiteSpace(name)) filterCondition.And(c => c.Name == name);

                return context.Roles
                                         .Where(filterCondition)
                                         .ToList();
            }
        }
    }
}
