﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Linq;
using LinqKit;
using QuanLyBanSach.Models;
using QuanLyBanSach.Data;
using Microsoft.EntityFrameworkCore;

namespace QuanLyBanSach.Services
{
    class BookServices
    {
        private static BookServices instance; 
        public static BookServices Instance
        {
            get { if (instance == null) instance = new BookServices(); return instance; }
            private set { BookServices.instance = value; }
        }
        private BookServices() { }

        public List<Book> GetAll()
        {
            using (var context = new QuanLyBanSachContext())
            {
                return context.Books
                                        .Include(x => x.AuthorNavigation)
                                        .Include(x => x.CategoryNavigation)
                                        .Include(x => x.PublisherNavigation)
                                        .ToList();  
            }
        }
        public void Insert(Book book)
        {
            using (var context = new QuanLyBanSachContext())
            {
                context.Books.Add(book);
                context.SaveChanges();
            }
        }
        public void Update(Book book)
        {
            using (var context = new QuanLyBanSachContext())
            {
                context.Books.Update(book);
                context.SaveChanges();
            }
        }
        public void Delete(Book book)
        {
            using (var context = new QuanLyBanSachContext())
            {
                context.Remove<Book>(book);
                context.SaveChanges();
            }
        }
        public List<Book> Filter(string name, string publisherID, string categoryID, string authorID)
        {
            using (var context = new QuanLyBanSachContext())
            {
                var filterCondition = PredicateBuilder.New<Book>();

                if (!String.IsNullOrWhiteSpace(name)) filterCondition.And(c => c.Name == name);
                if (!String.IsNullOrWhiteSpace(publisherID)) filterCondition.And(c => c.PublisherId.ToString() == publisherID);
                if (!String.IsNullOrWhiteSpace(categoryID)) filterCondition.And(c => c.CategoryId.ToString() == categoryID);
                if (!String.IsNullOrWhiteSpace(authorID)) filterCondition.And(c => c.AuthorId.ToString() == authorID);

                if (String.IsNullOrWhiteSpace(name) && String.IsNullOrWhiteSpace(publisherID) && String.IsNullOrWhiteSpace(categoryID) && String.IsNullOrWhiteSpace(authorID))
                {
                    filterCondition.And(c => true);
                }
                return context.Books
                                         .Where(filterCondition)
                                         .OrderBy(c => c.Name)
                                         .ToList();
            }
        }
    }
}
