﻿using LinqKit;
using QuanLyBanSach.Data;
using QuanLyBanSach.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace QuanLyBanSach.Services
{
    class CustomerServices
    {
        private static CustomerServices instance; 
        public static CustomerServices Instance
        {
            get { if (instance == null) instance = new CustomerServices(); return instance; }
            private set { CustomerServices.instance = value; }
        }
        private CustomerServices() {  }

        public List<Customer> GetAll()
        {
            using (var context = new QuanLyBanSachContext())
            {
                return context.Customers.OrderByDescending(x => x.CreateAt).ToList();
            }
        }
        public void Insert(Customer customer)
        {
            using (var context = new QuanLyBanSachContext())
            {
                context.Customers.Add(customer);
                context.SaveChanges();
            }
        }
        public void Update(Customer customer)
        {
            using (var context = new QuanLyBanSachContext())
            { 
                context.Customers.Update(customer);
                context.SaveChanges();
            }
        } 
        public void Delete(Customer customer)
        {
            using (var context = new QuanLyBanSachContext())
            {
                context.Customers.Remove(customer);
                context.SaveChanges();
            }
        }
        public List<Customer> Filter(string name, string phonenumber)
        {
            using (var context = new QuanLyBanSachContext())
            { 
                var filterCondition = PredicateBuilder.New<Customer>();

                if (!String.IsNullOrWhiteSpace(name)) filterCondition.And(c => c.Name == name);
                if (!String.IsNullOrWhiteSpace(phonenumber)) filterCondition.And(c => c.PhoneNumber == phonenumber);

                if (String.IsNullOrWhiteSpace(name) &&
                    String.IsNullOrWhiteSpace(phonenumber))
                {
                    filterCondition.And(c => true);
                }
                return context.Customers
                                         .Where(filterCondition)
                                         .OrderByDescending(c => c.CreateAt)
                                         .ToList();
            }
        }
    }
}
