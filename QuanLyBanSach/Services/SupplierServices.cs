﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Linq;
using LinqKit;
using QuanLyBanSach.Data;
using QuanLyBanSach.Models;

namespace QuanLyBanSach.Services
{
    class SupplierServices
    {
        private static SupplierServices instance; 
        public static SupplierServices Instance
        {
            get { if (instance == null) instance = new SupplierServices(); return instance; }
            private set { SupplierServices.instance = value; }
        }
        private SupplierServices() { }

        public List<Supplier> GetAll()
        {
            using (var context = new QuanLyBanSachContext())
            {
                return context.Suppliers.OrderByDescending(x => x.CreateAt).ToList();
            }
        }
        public void Insert(Supplier supplier)
        {
            using (var context = new QuanLyBanSachContext())
            {
                context.Suppliers.Add(supplier);
                context.SaveChanges();
            }
        }
        public void Update(Supplier supplier)
        {
            using (var context = new QuanLyBanSachContext())
            {
                context.Suppliers.Update(supplier);
                context.SaveChanges();
            }
        }
        public void Delete(Supplier supplier)
        {
            using (var context = new QuanLyBanSachContext())
            {
                context.Suppliers.Remove(supplier);
                context.SaveChanges();
            }
        }
        public List<Supplier> Filter(string name, string phonenumber)
        {
            using (var context = new QuanLyBanSachContext())
            {
                var filterCondition = PredicateBuilder.New<Supplier>();

                if (!String.IsNullOrWhiteSpace(name)) filterCondition.And(c => c.Name == name);
                if (!String.IsNullOrWhiteSpace(phonenumber)) filterCondition.And(c => c.PhoneNumber == phonenumber);

                if (String.IsNullOrWhiteSpace(name) &&
                    String.IsNullOrWhiteSpace(phonenumber))
                {
                    filterCondition.And(c => true);
                }
                return context.Suppliers
                                         .Where(filterCondition)
                                         .ToList();
            }
        }
    }
}
