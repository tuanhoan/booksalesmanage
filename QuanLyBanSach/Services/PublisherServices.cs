﻿using System;
using System.Collections.Generic;
using System.Text;
using QuanLyBanSach.Data;
using System.Linq;
using LinqKit;
using QuanLyBanSach.Models;

namespace QuanLyBanSach.Services
{
    class PublisherServices
    {
            
        private static PublisherServices instance; 
        public static PublisherServices Instance
        {
            get { if (instance == null) instance = new PublisherServices(); return instance; }
            private set { PublisherServices.instance = value; }
        }
        private PublisherServices() { }

        public List<Publisher> GetAll()
        {
            using (var context = new QuanLyBanSachContext())
            {
                return context.Publishers.OrderByDescending(x => x.CreateAt).ToList();
            }
        }
        public void Insert(Publisher publisher)
        {
            using (var context = new QuanLyBanSachContext())
            {
                context.Publishers.Add(publisher);
                context.SaveChanges();
            }
        }
        public void Update(Publisher publisher)
        {
            using (var context = new QuanLyBanSachContext())
            {
                context.Publishers.Update(publisher);
                context.SaveChanges();
            }
        }
        public void Delete(Publisher publisher)
        {
            using (var context = new QuanLyBanSachContext())
            {
                context.Publishers.Remove(publisher);
                context.SaveChanges();
            }
        }
        public List<Publisher> Filter(string name)
        {
            using (var context = new QuanLyBanSachContext())
            {
                var filterCondition = PredicateBuilder.New<Publisher>();

                if (!String.IsNullOrWhiteSpace(name)) filterCondition.And(c => c.Name == name);

                return context.Publishers
                                         .Where(filterCondition)
                                         .OrderByDescending(c => c.CreateAt)
                                         .ToList();
            }
        }
    }
}

