﻿using System;
using System.Collections.Generic;
using QuanLyBanSach.Data;
using QuanLyBanSach.Models;
using LinqKit;
using System.Text;
using System.Linq;
using Microsoft.EntityFrameworkCore;
using System.Security.Cryptography.X509Certificates;

namespace QuanLyBanSach.Services
{
    class AuthorServices
    {
        private static AuthorServices instance; 
        public static AuthorServices Instance
        {
            get { if (instance == null) instance = new AuthorServices(); return instance; }
            private set { AuthorServices.instance = value; }
        }
        private AuthorServices() { }

        public List<Author> GetAll()
        {
            using(var context = new QuanLyBanSachContext())
            {
                return context.Authors.OrderByDescending(x => x.CreateAt).ToList();
            }
        }
        public void Insert(Author author)
        {
            using (var context = new QuanLyBanSachContext())
            {
                context.Authors.Add(author);
                context.SaveChanges();
            } 
        }
        public void Update(Author author)
        {
            using (var context = new QuanLyBanSachContext())
            {
                context.Authors.Update(author);
                context.SaveChanges();
            }
            
        }
        public void Delete(Author author)
        {
            using (var context = new QuanLyBanSachContext())
            {
                context.Authors.Remove(author);
                context.SaveChanges();
            } 
        }
        public List<Author> Filter(string name)
        {
            using (var context = new QuanLyBanSachContext())
            { 
                var filterCondition = PredicateBuilder.New<Author>();

                if (!String.IsNullOrWhiteSpace(name)) filterCondition.And(c => c.Name == name);

                if (String.IsNullOrWhiteSpace(name))
                {
                    filterCondition.And(c => true);
                }
                return context.Authors
                                         .Where(filterCondition)
                                         .OrderByDescending(c => c.CreateAt)
                                         .ToList();
            } 
        }
    }
}
